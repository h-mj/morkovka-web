export default {
  calories: "Калории",
  carbs: "Углеводы",
  proteins: "Белки",
  fats: "Жиры",

  g: "г",
  kg: "кг",
  pc: "шт",
  ml: "мл",
  cm: "см",

  registration: {
    language: "Язык",
    name: "Имя",
    sex: "Пол",
    date: "Число",
    month: "Месяц",
    year: "Год",
    email: "Адрес электронной почты",
    password: "Пароль",
    code: "Код",
    register: "Зарегистрироваться",
    emailUsed: "Этот адрес электронной почты уже используется."
  },

  user: {
    female: "Женщина",
    male: "Мужчина",
    sex: "Пол",
    age: "Возраст"
  },

  progress: {
    left: "осталось",
    under: "недоедено",
    over: "превышено"
  },

  days: [
    "Воскресенье",
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота"
  ],

  navigation: {
    counter: "Ввод",
    quantities: "Замеры",
    progress: "Прогресс",
    history: "История",
    clients: "Клиенты",
    settings: "Настройки",
    logout: "Выйти"
  },

  title: {
    bmi: "Индекс массы тела",
    calories: "Дневной калораж",
    carbs: "Дневное количество углеводов",
    proteins: "Дневное количество белков",
    fats: "Дневное количество жиров"
  },

  bmi: {
    noData: "Добавьте значения массы, высоты и активности."
  },

  bmiRanges: {
    0: "Выраженный дефицит массы",
    1: "Недостаточная масса тела",
    2: "Норма",
    3: "Предожирение",
    4: "Ожирение первой степени",
    5: "Ожирение второй степени",
    6: "Ожирение третьей степени"
  },

  name: "Название",
  quantity: "Количество",
  unit: "Едини́ца",
  add: "Добавить",
  update: "Обновить",
  back: "Назад",
  cancel: "Отмена",
  saved: "Сохранено",

  addMeal: {
    breakfast: "Добавить завтрак",
    lunch: "Добавить обед",
    dinner: "Добавить ужин",
    snack: "Добавить закуску"
  },

  deleteAccount: "Удалить аккаунт",
  foodstuffAdd: "Добавить продукт",
  quantityAdd: "Добавить размер",
  consumptionData: "Детали",
  generatedCode: "Сгенерированная код",

  label: {
    calories: "Энергия",
    carbs: "Углеводы (г)",
    proteins: "Белки (г)",
    fats: "Жиры (г)",
    sugars: "Сахар (г)",
    salt: "Соль (г)",
    saturates: "Насыщенные жирные кислоты (г)"
  },

  activeness: {
    minimal: "Минимальная",
    low: "Низкая",
    medium: "Средняя",
    high: "Высокая",
    veryHigh: "Очень высокая"
  },

  mealNames: {
    0: "Завтрак",
    1: "Обед",
    2: "Ужин",
    3: "Закуска"
  },

  quantityNames: {
    1: "Масса",
    2: "Рост",
    3: "Активность",
    4: "Oбъем груди",
    5: "Oбъем талии",
    6: "Oбъем бедер",
    7: "Oбъем бедра",
    8: "Oбъем икры",
    9: "Oбъем бицепса",
    10: "Oбъем запястья",
    11: "Oбъем голени"
  },

  confirm: {
    yes: "Да",
    no: "Нет",
    food: "Уверены ли вы, что хотите удалить этот продукт?",
    foodstuff: "Уверены ли вы, что хотите удалить этот продукт?",
    quantity: "Уверены ли вы, что хотите удалить этот размер?",
    measurement: "Уверены ли вы, что хотите удалить этот замер?",
    user: "Уверены ли вы, что хотите удалить свой аккаунт?",
    client: "Уверены ли вы, что хотите удалить аккаунт клиента?"
  },

  ratioChanger: {
    calories: "Калораж",
    change: "Процент рациона"
  },

  measurement: {
    date: "Дата",
    value: "Данные"
  },

  settings: {
    language: "Язык",
    email: "Адрес электронной почты",
    newPassword: "Новый пароль",
    repeatPassword: "Повторите пароль",
    currentPassword: "Пароль",
    save: "Сохранить"
  }
};
