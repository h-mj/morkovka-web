export default {
  calories: "Kalorid",
  carbs: "Süsivesikud",
  proteins: "Valgud",
  fats: "Rasvad",

  g: "g",
  kg: "kg",
  pc: "tk",
  ml: "ml",
  cm: "cm",

  registration: {
    language: "Keel",
    name: "Täisnimi",
    sex: "Sugu",
    date: "Kuupäev",
    month: "Kuu",
    year: "Aasta",
    email: "E-posti aadress",
    password: "Parool",
    code: "Kood",
    register: "Registreeru",
    emailUsed: "See e-posti aadress on juba kasutuses."
  },

  user: {
    female: "Naine",
    male: "Mees",
    sex: "Sugu",
    age: "Vanus"
  },

  progress: {
    left: "alles",
    under: "puudu",
    over: "üle"
  },

  days: [
    "Pühapäev",
    "Esmaspäev",
    "Teisipäev",
    "Kolmapäev",
    "Neljapäev",
    "Reede",
    "Laupäev"
  ],

  navigation: {
    counter: "Sisesta",
    quantities: "Mõõdud",
    progress: "Progress",
    history: "Ajalugu",
    clients: "Kliendid",
    settings: "Sätted",
    logout: "Logi välja"
  },

  title: {
    bmi: "Kehamassiindeks",
    calories: "Päevane kaloraaž",
    carbs: "Päevane süsivesikute kogus",
    proteins: "Päevane valkude kogus",
    fats: "Päevane rasvade kogus"
  },

  bmi: {
    noData: "Palun lisa massi, pikkuse ja aktiivsuse väärtused."
  },

  bmiRanges: {
    0: "Tõsine alakaal",
    1: "Alakaal",
    2: "Normaalkaal",
    3: "Ülekaal",
    4: "Rasvumise 1. klass",
    5: "Rasvumise 2. klass",
    6: "Rasvumise 3. klass"
  },

  name: "Nimetus",
  quantity: "Kogus",
  unit: "Ühik",
  add: "Lisa",
  update: "Uuenda",
  back: "Tagasi",
  cancel: "Tühista",
  saved: "Salvestatud",

  addMeal: {
    breakfast: "Lisa hommikusöök",
    lunch: "Lisa lõunasöök",
    dinner: "Lisa õhtusöök",
    snack: "Lisa vahepealne eine"
  },

  deleteAccount: "Kustuta konto",
  foodstuffAdd: "Lisa toiduaine",
  quantityAdd: "Lisa suurus",
  consumptionData: "Detailid",
  generatedCode: "Genereeritud kood",

  label: {
    calories: "Energia",
    carbs: "Süsivesikud (g)",
    proteins: "Valgud (g)",
    fats: "Rasvad (g)",
    sugars: "Suhkrud (g)",
    salt: "Sool (g)",
    saturates: "Küllastunud rasvhapped (g)"
  },

  activeness: {
    minimal: "Minimaalne",
    low: "Madal",
    medium: "Keskmine",
    high: "Kõrge",
    veryHigh: "Väga kõrge"
  },

  mealNames: {
    0: "Hommikusöök",
    1: "Lõunasöök",
    2: "Õhtusöök",
    3: "Vahepealne eine"
  },

  quantityNames: {
    1: "Mass",
    2: "Pikkus",
    3: "Aktiivsus",
    4: "Rinnaümbermõõt",
    5: "Taljeümbermõõt",
    6: "Reiteümbermõõt",
    7: "Reieümbermõõt",
    8: "Sääremarjaümbermõõt",
    9: "Biitseptsiümbermõõt",
    10: "Käevarreümbermõõt",
    11: "Pahkluuümbermõõt"
  },

  confirm: {
    yes: "Jah",
    no: "Ei",
    food: "Olete kindel, et soovite söögi kustutada?",
    foodstuff: "Olete kindel, et soovite toiduaine kustutada?",
    quantity: "Olete kindel, et soovite suuruse kustutada?",
    measurement: "Olete kindel, et soovite mõõdu kustutada?",
    user: "Olete kindet, et soovite konto kustutada?",
    client: "Olete kindel, et soovite kliendi konto kustutada?"
  },

  ratioChanger: {
    calories: "Kaloraaž",
    change: "Muut (%)"
  },

  measurement: {
    date: "Kuupäev",
    value: "Väärtus"
  },

  settings: {
    language: "Keel",
    email: "E-posti aadress",
    newPassword: "Uus parool",
    repeatPassword: "Korda parooli",
    currentPassword: "Parool",
    save: "Salvesta"
  }
};
