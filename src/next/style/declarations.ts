interface IDeclarations {
  borderColor: IBorderColorDeclarations;
  color: IColorDeclarations;
}

interface IBorderColorDeclarations {
  default: string;
  defaultAction: string;
  active: string;
  error: string;
  errorAction: string;
}

interface IColorDeclarations {
  default: string;
  defaultAction: string;
  active: string;
  error: string;
  errorAction: string;
}

export const declarations: IDeclarations = {
  borderColor: {
    default: "rgba(0, 0, 0, 0.35)",
    defaultAction: "rgba(0, 0, 0, 0.60)",
    active: "rgba(0, 0, 0, 0.65)",
    error: "rgba(255, 0, 0, 0.65)",
    errorAction: "rgba(255, 0, 0.75)"
  },
  color: {
    default: "rgba(0, 0, 0, 0.55)",
    defaultAction: "rgba(0, 0, 0, 0.80)",
    active: "rgba(0, 0, 0, 0.95)",
    error: "rgba(255, 0, 0, 0.95)",
    errorAction: "rgba(255, 0, 0, 1)"
  }
};
