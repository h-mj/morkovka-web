import * as React from "react";
import styled, { css } from "styled-components";
import { declarations } from "../style/declarations";

interface IInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  error?: string;
}

export class Input extends React.Component<IInputProps> {
  public render() {
    const { placeholder, error, ...props } = this.props;

    return (
      <Wrapper>
        <Element hasError={!!error} {...props} />
        <Placeholder>{placeholder}</Placeholder>
        <Error>{error}</Error>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  height: 5.5rem;
  position: relative;
`;

const Placeholder = styled.div`
  position: absolute;
  top: 1rem;
  line-height: 2.5rem;
  font-size: 1rem;
  pointer-events: none;
  transition: 0.25s;
`;

const placeholderLabelStyle = css`
  top: 0;
  line-height: 1rem;
  font-size: 0.8rem;
`;
const Error = styled.div`
  position: absolute;
  top: 4rem;
  line-height: 1rem;
  font-size: 0.8rem;
  transition: 0.25s;
`;

interface IElementProps {
  hasError?: boolean;
}

const elementDefaultStyle = css`
  color: ${declarations.color.default};
  border-color: ${declarations.borderColor.default};

  & + ${Placeholder} {
    color: ${declarations.color.default};
  }

  & + ${Placeholder} + ${Error} {
    opacity: 0;
  }

  &:hover {
    color: ${declarations.color.defaultAction};
    border-color: ${declarations.borderColor.defaultAction};
  }

  &:hover + ${Placeholder} {
    color: ${declarations.color.defaultAction};
  }
`;

const elementErrorStyle = css`
  color: ${declarations.color.error};
  border-color: ${declarations.borderColor.error};

  & + ${Placeholder} {
    color: ${declarations.color.error};
  }

  & + ${Placeholder} + ${Error} {
    opacity: 1;
    color: ${declarations.color.error};
  }

  &:hover {
    color: ${declarations.color.errorAction};
    border-color: ${declarations.borderColor.errorAction};
  }

  &:hover + ${Placeholder} {
    color: ${declarations.color.errorAction};
  }

  &:hover + ${Placeholder} + ${Error} {
    color: ${declarations.color.errorAction};
  }
`;

const Element = styled.input<IElementProps>`
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  background: none;
  width: 100%;

  position: absolute;
  top: 1rem;
  line-height: 2.5rem;

  border-bottom: solid 2px;

  transition: 0.25s;

  ${elementDefaultStyle};

  &:focus {
    color: ${declarations.color.active};
    border-color: ${declarations.borderColor.active};
  }

  &:focus + ${Placeholder} {
    ${placeholderLabelStyle};
    color: ${declarations.color.active};
  }

  ${props => props.value && `& + ${Placeholder} {${placeholderLabelStyle}}`};

  ${props => props.hasError && elementErrorStyle};
`;
