import * as React from "react";
import styled, { css } from "styled-components";
import { declarations } from "../style/declarations";

interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  hasError?: boolean;
}

export class Button extends React.Component<IButtonProps> {
  public render() {
    return <Element {...this.props} />;
  }
}

const elementDefaultStyle = css`
  color: ${declarations.color.default};
  border-color: ${declarations.borderColor.default};

  &:hover {
    color: ${declarations.color.defaultAction};
    border-color: ${declarations.borderColor.defaultAction};
  }
`;

const elementErrorStyle = css`
  color: ${declarations.color.error};
  border-color: ${declarations.borderColor.error};

  &:hover {
    color: ${declarations.color.errorAction};
    border-color: ${declarations.borderColor.errorAction};
  }
`;

interface IElementProps {
  hasError?: boolean;
}

const Element = styled.button<IElementProps>`
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  background: none;

  width: 100%;
  height: 3.5rem;

  display: flex;
  align-items: center;
  justify-content: center;

  border: solid 2px;
  border-radius: 0.5rem;

  cursor: pointer;

  transition: 0.25s;

  ${elementDefaultStyle};

  &:active {
    color: ${declarations.color.active};
    border-color: ${declarations.borderColor.active};
  }

  ${props => props.hasError && elementErrorStyle};
`;
