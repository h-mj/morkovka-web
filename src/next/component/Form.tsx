import * as React from "react";
import { observer } from "mobx-react";

import { Input } from "./Input";
import { observable } from "mobx";
import { Button } from "./Button";

interface IFieldDefinition {
  type: string;
  placeholder: string;
}

interface IFieldDefinitions {
  email: IFieldDefinition;
  password: IFieldDefinition;
}

const definitions: IFieldDefinitions = {
  email: { type: "email", placeholder: "E-posti aadress" },
  password: { type: "password", placeholder: "Parool" }
};

type IFieldData = { [key in keyof IFieldDefinitions]?: string };

interface IFormProps {
  fields: Array<keyof IFieldDefinitions>;
  onChange?: (field: keyof IFieldDefinitions, value: string) => any;
  onSubmit?: (data: IFieldData) => any;
}

@observer
export class Form extends React.Component<IFormProps> {
  @observable
  private data: IFieldData;

  constructor(props: IFormProps) {
    super(props);

    this.data = {};
    for (const field of props.fields) {
      this.data[field] = "";
    }
  }

  public render() {
    const { fields } = this.props;

    return (
      <form noValidate onSubmit={this.handleSubmit}>
        {fields.map(field => (
          <Input
            key={field}
            name={field}
            type={definitions[field].type}
            placeholder={definitions[field].placeholder}
            value={this.data[field]!}
            onChange={this.handleChange}
          />
        ))}

        <Button type="submit">Submit</Button>
      </form>
    );
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    const field = name as keyof IFieldDefinitions;

    this.data[field]! = value;

    if (this.props.onChange) {
      this.props.onChange(field, value);
    }
  };

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (this.props.onSubmit) {
      this.props.onSubmit(this.data);
    }
  };
}
