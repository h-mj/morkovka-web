import React from "react";
import styled from "styled-components";
import { observer } from "mobx-react";

import Quantity from "./components/Quantity";
import QuantityPlus from "./components/QuantityPlus";

function QuantityList({ quantities, showDetails, showAdder }) {
  const tiles = quantities.quantities.map(quantity => (
    <Quantity
      key={quantity.id.toString()}
      quantity={quantity}
      showDetails={showDetails}
    />
  ));

  tiles.unshift(
    <QuantityPlus key="TileAdd" onClick={showAdder}>
      +
    </QuantityPlus>
  );

  return <Tiles>{tiles}</Tiles>;
}

const Tiles = styled.div`
  width: 20rem;
  height: 100%;
  overflow: auto;
  flex-shrink: 0;
  background-color: white;
`;

export default observer(QuantityList);
