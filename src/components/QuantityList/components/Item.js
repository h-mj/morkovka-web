import styled from "styled-components";
import styles from "../../../styles/constants";

export default styled.div`
  width: 100%;
  height: 6rem;
  position: relative;
  color: ${styles.color.text};
  border-bottom: solid 1px ${styles.borderColor.gray};

  &:last-child {
    border-bottom: 0;
  }

  cursor: pointer;
  user-select: none;
  transition: 0.2s;

  &:hover {
    background-color: #f5f5f5;
  }
`;
