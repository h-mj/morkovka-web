import React from "react";
import styled from "styled-components";
import { inject, observer } from "mobx-react";

import Remove from "../../Remove";
import Item from "./Item";

function Quantity({ t9n, quantity, showDetails }) {
  return (
    <Item onClick={() => showDetails(quantity)}>
      <Center>
        <Aligner>
          {quantity.value !== null && <Value>{quantity.prettyValue}</Value>}
          {quantity.prettyName}
        </Aligner>
      </Center>

      {quantity.deletable && (
        <RemoveContainer>
          <Remove
            message={t9n.t.confirm.quantity}
            remove={() => quantity.delete()}
          />
        </RemoveContainer>
      )}
    </Item>
  );
}

const Center = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Aligner = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const Value = styled.div`
  font-size: 1.8rem;
  margin-bottom: 0.5rem;
`;

const RemoveContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  padding: 1rem;
`;

export default inject("t9n")(observer(Quantity));
