import styled from "styled-components";

import styles from "../../../styles/constants";

import Item from "./Item";

export default styled(Item)`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 2.5rem;
  height: 6rem;
  color: ${styles.color.text};
`;
