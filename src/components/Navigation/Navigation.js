import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import styles from "../../styles/constants";

import Flex from "../Flex";

class Navigation extends Component {
  render() {
    const { user } = this.props.auth;

    if (user === null) {
      return <Container />;
    }

    return (
      <Container>
        <Section>
          <Item to="/counter">{this.props.t9n.t.navigation.counter}</Item>
          <Item to="/quantities">{this.props.t9n.t.navigation.quantities}</Item>
          <Item to="/progress">{this.props.t9n.t.navigation.progress}</Item>
          <Item to="/history">{this.props.t9n.t.navigation.history}</Item>
          {user.type === 1 && (
            <Item to="/clients">{this.props.t9n.t.navigation.clients}</Item>
          )}
        </Section>

        <Section>
          <Item to="/settings">{this.props.t9n.t.navigation.settings}</Item>
          <Item to="/logout" onClick={this.logout}>
            {this.props.t9n.t.navigation.logout}
          </Item>
        </Section>
      </Container>
    );
  }

  logout = event => {
    event.preventDefault();
    this.props.auth.logout();
  };
}

const Container = styled(Flex)`
  flex-shrink: 0;
  width: 100%;
  overflow-x: auto;
  min-height: 3.5rem;
  justify-content: space-between;
  background-color: ${styles.backgroundColor.slate};
`;

const Section = styled.div`
  display: flex;
`;

const Item = styled(Link)`
  line-height: 3.5rem;
  padding: 0 1.5rem;
  text-decoration: none;
  color: ${styles.color.text};

  cursor: pointer;
  user-select: none;
  transition: 0.2s;

  &:hover {
    color: ${styles.color.textDark};
  }
`;

export default inject("auth", "t9n")(observer(Navigation));
