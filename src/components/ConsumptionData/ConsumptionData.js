import React from "react";
import { inject, observer } from "mobx-react";

import Modal from "../Modal";
import ModalHeader from "../ModalHeader";
import Body from "../Body";
import Meal from "../Meal";
import Progresses from "../Progresses";

function ConsumptionData({ t9n, date, user, close }) {
  return (
    <Modal>
      {user.day && (
        <Body>
          <ModalHeader title={t9n.t.consumptionData} close={close} />

          <Progresses day={user.day} past />

          {user.day.meals.meals.map(meal => (
            <Meal key={meal.id.toString()} meal={meal} readOnly />
          ))}
        </Body>
      )}
    </Modal>
  );
}

export default inject("t9n")(observer(ConsumptionData));
