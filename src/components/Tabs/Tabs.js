import React from "react";
import styled from "styled-components";
import styles from "../../styles/constants";

export default function Tabs({ tabs, selected, select }) {
  return (
    <Container>
      <TabPlacer>
        <TabList selected={selected}>
          {tabs.map((value, index) => (
            <Tab
              key={index.toString()}
              selected={index === selected}
              onClick={() => select(index)}
            >
              {value}
            </Tab>
          ))}
        </TabList>
      </TabPlacer>
    </Container>
  );
}

const Container = styled.div`
  width: 100%;
  overflow: hidden;
  background-color: white;
`;

const TabPrototype = styled.div`
  width: 15rem;
  height: 3.5rem;
`;

const TabPlacer = styled(TabPrototype)`
  margin: 0 auto;
  position: relative;
`;

const TabList = styled.div`
  display: flex;
  position: absolute;
  left: ${props => props.selected * -100 + "%"};
  transition: 0.2s;
`;

const Tab = styled(TabPrototype)`
  flex-shrink: 0;
  color: ${props =>
    (props.selected && styles.color.textDark) || styles.color.textLight};

  display: flex;
  align-items: center;
  justify-content: center;

  cursor: pointer;
  user-select: none;
  transition: 0.2s;

  &:hover {
    color: ${styles.color.textDark};
  }
`;
