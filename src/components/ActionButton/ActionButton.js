import styled from "styled-components";

import styles from "../../styles/constants";

import Button from "../Button";

export default styled(Button)`
  color: white;
  background-color: ${styles.backgroundColor.primary};

  &:hover {
    background-color: ${styles.backgroundColor.primaryAction};
  }
`;
