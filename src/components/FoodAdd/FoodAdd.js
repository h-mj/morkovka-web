import React, { Component } from "react";
import styled from "styled-components";
import styles from "../../styles/constants";
import { decorate, observable, action } from "mobx";
import { inject, observer } from "mobx-react";

import Input from "../Input";
import Button from "../Button";
import ActionButton from "../ActionButton";
import Container from "../Container";
import Modal from "../Modal";
import FoodstuffAdd from "../FoodstuffAdd";

import Labels from "./components/Labels";
import Foodstuff from "./components/Foodstuff";
import Add from "./components/Add";

class FoodAdd extends Component {
  constructor(props) {
    super(props);

    this.query = "";
    this.quantity = "";
    this.loaded = true;
    this.foodstuffs = [];
    this.foodstuff = null;
    this.adding = false;

    this.invalid = {
      query: false,
      quantity: false
    };
  }

  render() {
    return (
      <Modal>
        <Container>
          <Form onSubmit={this.submit} noValidate>
            <Input
              onChange={this.find}
              name="query"
              autoFocus
              autoComplete="off"
              value={this.query}
              invalid={this.invalid.query}
              placeholder={this.props.t9n.t.name}
            />

            <Section>
              <Input
                onChange={event => {
                  this.quantity = event.target.value;
                }}
                name="quantity"
                value={this.quantity}
                invalid={this.invalid.quantity}
                placeholder={this.props.t9n.t.quantity}
                type="number"
              />
            </Section>

            <Unit>
              {this.foodstuff && this.props.t9n.t[this.foodstuff.unit.trim()]}
            </Unit>

            <Section>
              <ActionButton type="submit">{this.props.t9n.t.add}</ActionButton>
            </Section>

            <Section>
              <Button type="button" onClick={this.close}>
                {this.props.t9n.t.back}
              </Button>
            </Section>
          </Form>

          {this.query && this.foodstuff === null && (
            <div>
              {this.foodstuffs.length > 0 && <Labels />}

              {this.foodstuffs.map((value, index) => (
                <Foodstuff
                  key={value.name + "@" + value.unit}
                  foodstuff={value}
                  use={this.use}
                  remove={this.remove}
                />
              ))}

              {(this.loaded || this.foodstuffs.length > 0) && (
                <Add onClick={this.openFoodstuff}>
                  {this.props.t9n.t.foodstuffAdd}
                </Add>
              )}
            </div>
          )}
        </Container>
        {this.adding && (
          <FoodstuffAdd use={this.use} close={this.closeFoodstuff} />
        )}
      </Modal>
    );
  }

  openFoodstuff = () => {
    this.adding = true;
  };

  closeFoodstuff = () => {
    this.adding = false;
  };

  clear = () => {
    this.query = "";
    this.quantity = "";
    this.foodstuff = null;
    this.foodstuffs = [];
    this.loaded = true;
    this.adding = false;
    this.setValid([], true);
  };

  close = () => {
    this.clear();
    this.props.close();
  };

  setValid = (names, valid) => {
    if (!(names instanceof Array)) {
      names = [names];
    }

    for (const key in this.invalid) {
      this.invalid[key] = false;
    }

    for (let i = 0; i < names.length; ++i) {
      this.invalid[names[i]] = !valid;
    }
  };

  remove = foodstuff => {
    foodstuff.delete();
    this.foodstuffs = [];
    this.find(null, true);
  };

  find = (event, previous = false) => {
    this.foodstuff = null;
    this.loaded = false;

    if (!previous) {
      this.query = event.target.value;
    }

    if (!this.query) {
      return;
    }

    this.props.auth.foodstuffs
      .find(this.query)
      .then(data => {
        this.foodstuffs = data;
        this.loaded = true;
      })
      .catch(error => {
        if (error.code === 422) {
          this.foodstuffs = [];
        } else {
          console.log(error);
        }

        this.loaded = true;
      });
  };

  use = foodstuff => {
    this.foodstuff = foodstuff;
    this.query = foodstuff.name;

    this.setValid("query", true);
  };

  submit = event => {
    event.preventDefault();

    if (!this.foodstuff) {
      return this.setValid("query", false);
    }

    if (!this.quantity) {
      return this.setValid("quantity", false);
    }

    this.props.meal
      .add(this.foodstuff.id, this.quantity)
      .then(() => {
        this.close();
      })
      .catch(error => {
        if (error.code === 422) {
          this.setValid(error.details.invalid, false);
        } else {
          console.error(error);
        }
      });
  };
}

decorate(FoodAdd, {
  query: observable,
  quantity: observable,
  loaded: observable,
  foodstuffs: observable,
  foodstuff: observable,
  adding: observable,
  invalid: observable,
  openFoodstuff: action,
  closeFoodstuff: action,
  setValid: action,
  find: action,
  use: action,
  submit: action
});

const Form = styled.form`
  display: flex;
  height: 3.5rem;
`;

const Section = styled.div`
  width: 5.5rem;
  flex-shrink: 0;
`;

const Unit = styled.div`
  width: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  line-height: 3.5rem;
  color: ${styles.color.text};
  padding: 0 1rem;
`;

export default inject("auth", "t9n")(observer(FoodAdd));
