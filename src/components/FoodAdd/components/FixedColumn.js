import styled from "styled-components";

import Column from "./Column";

export default styled(Column)`
  max-width: 5rem;
  width: 12%;
  flex-shrink: 0;
  justify-content: center;
  white-space: nowrap;
`;
