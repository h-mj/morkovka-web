import React from "react";
import styled from "styled-components";
import styles from "../../../styles/constants";
import { inject, observer } from "mobx-react";

import Flex from "../../Flex";
import Column from "./Column";
import FixedColumn from "./FixedColumn";
import ThinColumn from "./ThinColumn";
import Remove from "../../Remove";

function Foodstuff({ auth, t9n, foodstuff, use, remove }) {
  console.log("Hello", foodstuff);

  return (
    <FoodstuffRow onClick={() => use(foodstuff)}>
      <FixedColumn>{foodstuff.quantityWithUnit}</FixedColumn>
      <Column>{foodstuff.name}</Column>
      <FixedColumn>{foodstuff.caloriesWithUnit}</FixedColumn>
      <FixedColumn>{foodstuff.carbsWithUnit}</FixedColumn>
      <FixedColumn>{foodstuff.proteinsWithUnit}</FixedColumn>
      <FixedColumn>{foodstuff.fatsWithUnit}</FixedColumn>

      {auth.user.type === 1 && <ThinColumn>
        <Remove message={t9n.t.confirm.foodstuff} remove={() => remove(foodstuff)} />
      </ThinColumn>}
    </FoodstuffRow>
  );
}

const FoodstuffRow = styled(Flex)`
  border-bottom: solid 1px ${styles.borderColor.gray};

  cursor: pointer;
  user-select: none;

  &:hover {
    background-color: #f5f5f5;
  }

  &:last-child {
    border-bottom: 0;
  }
`;

export default inject("auth", "t9n")(observer(Foodstuff));
