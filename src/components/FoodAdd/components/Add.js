import styled from "styled-components";
import styles from "../../../styles/constants";

export default styled.div`
  width: 100%;
  line-height: 3.5rem;
  text-align: center;
  color: ${styles.color.text};

  cursor: pointer;
  user-select: none;
  transition: 0.2s;

  &:hover {
    color: ${styles.color.textDark};
    background-color: #f5f5f5;
  }
`;
