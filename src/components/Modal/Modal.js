import React from "react";
import styled from "styled-components";

import Body from "../Body";

export default function Modal({ children }) {
  return (
    <Container onClick={event => event.stopPropagation()}>
      <Body>{children}</Body>
    </Container>
  );
}

const Container = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  position: fixed;
  z-index: 1000;
  overflow: auto;
  margin: 0;
  padding: 0;
  background-color: #f5f5f5;
`;
