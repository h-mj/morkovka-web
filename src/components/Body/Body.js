import styled from "styled-components";
import fadeIn from "../../styles/fadeIn";

export default styled.div`
  max-width: 60rem;
  width: 100%;
  margin: 0 auto;
  padding: 4rem 1rem;
  box-sizing: border-box;
  animation: ${fadeIn} 0.2s;

  @media screen and (max-width: 40rem) {
    padding-top: 1rem;
  }
`;
