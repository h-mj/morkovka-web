import React from "react";
import styled, { css } from "styled-components";
import styles from "../../styles/constants";

export default function Input({
  value,
  placeholder,
  disabled,
  noAnimation,
  ...rest
}) {
  return (
    <Div>
      <ActualInput
        value={value}
        disabled={disabled}
        placeholder={noAnimation && placeholder}
        {...rest}
      />

      {placeholder && !noAnimation && (
        <Placeholder
          valueExists={!!value}
          disabled={disabled}
          noAnimation={noAnimation}
        >
          {placeholder}
        </Placeholder>
      )}
    </Div>
  );
}

const Div = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

const ActualInput = styled.input`
  width: 100%;

  border: none;
  outline: none;
  margin: 0;
  padding: 0;
  background: none;

  color: ${styles.color.text};
  padding: 1rem;
  line-height: 1.5rem;
  box-sizing: border-box;
  background-color: white;

  &::placeholder,
  &:disabled {
    color: ${styles.color.textLight};
  }

  ${props => props.invalid && "background-color: #ffdddd"};
`;

const smallText = css`
  padding-top: 0.5rem;
  font-size: 0.8rem;
  line-height: 0;
  color: ${styles.color.text};
`;

const Placeholder = styled.div`
  color: ${styles.color.textLight};

  padding: 1rem;
  line-height: 1.5rem;
  position: absolute;
  pointer-events: none;
  top: 0;
  left: 0;
  transition: 0.2s;

  ${ActualInput}:focus ~ & {
    ${smallText};
  }

  ${props => props.valueExists && smallText};
`;
