import React, { Component } from "react";
import styled from "styled-components";
import styles from "../../../styles/constants";
import { action, decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";

import Input from "../../Input";
import Select from "../../Select";
import ActionButton from "../../ActionButton";

import Quantity from "../../../models/Quantity";

class MeasurementAdd extends Component {
  constructor(props) {
    super(props);

    this.value = "";
    this.invalid = false;
  }

  render() {
    const quantity = this.props.quantity;

    if (quantity.type === Quantity.ACTIVENESS) {
      return (
        <Form onSubmit={this.update}>
          <Select
            placeholder={quantity.prettyName}
            value={this.value}
            invalid={this.invalid}
            onChange={this.change}
            options={[
              { value: 0, name: this.props.t9n.t.activeness.minimal },
              { value: 1, name: this.props.t9n.t.activeness.low },
              { value: 2, name: this.props.t9n.t.activeness.medium },
              { value: 3, name: this.props.t9n.t.activeness.high },
              { value: 4, name: this.props.t9n.t.activeness.veryHigh }
            ]}
          />
        </Form>
      );
    }

    return (
      <Form onSubmit={this.update} noValidate>
        <Input
          placeholder={quantity.prettyName}
          value={this.value}
          invalid={this.invalid}
          onChange={this.change}
          innerRef={input => input && input.focus()}
          noAnimation
          type={"number"}
        />

        <Unit>{quantity.prettyUnit}</Unit>

        <NarrowActionButton>{this.props.t9n.t.update}</NarrowActionButton>
      </Form>
    );
  }

  clear = () => {
    this.value = "";
    this.invalid = false;
  };

  change = event => {
    this.value = event.target.value;

    if (this.props.quantity.type === Quantity.ACTIVENESS) {
      this.update();
    }
  };

  update = event => {
    if (event) {
      event.preventDefault();
    }

    if (this.value === "") {
      this.invalid = true;
      return;
    }

    this.props.quantity
      .add(this.value)
      .then(() => {
        this.clear();
      })
      .catch(error => {
        if (error.code === 422) {
          this.invalid = true;
        } else {
          console.error(error);
        }
      });
  };
}

decorate(MeasurementAdd, {
  value: observable,
  invalid: observable,
  change: action,
  update: action
});

const Form = styled.form`
  display: flex;
  width: 100%;
  box-sizing: border-box;
  height: 3.5rem;
  background-color: white;
  box-shadow: ${styles.boxShadow._1};
`;

const NarrowActionButton = styled(ActionButton)`
  width: 8rem;
  flex-shrink: 0;
`;

const Unit = styled.div`
  line-height: 3.5rem;
  color: ${styles.color.text};
  padding: 0 1rem;
`;

export default inject("t9n")(observer(MeasurementAdd));
