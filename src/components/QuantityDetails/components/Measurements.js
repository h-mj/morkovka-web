import React from "react";
import styled from "styled-components";
import { inject, observer } from "mobx-react";
import styles from "../../../styles/constants";
import moment from "moment";

import Flex from "../../Flex";
import Container from "../../Container";
import Remove from "../../Remove";

import Quantity from "../../../models/Quantity";

function Measurements({ t9n, measurements }) {
  return (
    <Div>
      <Flex>
        <TitleColumn>{t9n.t.measurement.date}</TitleColumn>
        <TitleColumn>{t9n.t.measurement.value}</TitleColumn>
        <RemoveColumn />
      </Flex>

      {measurements.map(measurement => (
        <Flex key={measurement.id.toString()}>
          <Column>{moment(measurement.date).format("DD.MM.YYYY")}</Column>
          <Column>
            {measurement.quantity.type === Quantity.ACTIVENESS
              ? t9n.t.activeness[Quantity.ACTIVENESS_NAMES[measurement.value]]
              : measurement.value}
          </Column>

          <RemoveColumn>
            <Remove
              remove={measurement.delete}
              message={t9n.t.confirm.measurement}
            />
          </RemoveColumn>
        </Flex>
      ))}
    </Div>
  );
}

const Div = styled(Container)`
  max-width: 30rem;
  margin: auto;
  color: ${styles.color.text};
`;

const Column = styled.div`
  width: 100%;
  padding: 0 1rem;
  line-height: 3.5rem;
  text-align: center;
`;

const RemoveColumn = styled(Column)`
  width: 2.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const TitleColumn = styled(Column)`
  font-weight: bold;
`;

export default inject("t9n")(observer(Measurements));
