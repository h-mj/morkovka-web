import React from "react";
import { observer } from "mobx-react";

import Body from "../Body";
import Section from "../Section";
import MeasurementChart from "../MeasurementChart";

import MeasurementAdd from "./components/MeasurementAdd";
import Measurements from "./components/Measurements";

function QuantityDetails({ quantity, close }) {
  if (quantity.measurements === null) {
    return null;
  }

  return (
    <Body>
      <Section>
        <MeasurementAdd quantity={quantity} />
      </Section>

      {quantity.measurements.length > 0 && (
        <Body>
          <Section>
            <MeasurementChart quantity={quantity} />
          </Section>

          <Section>
            <Measurements measurements={quantity.measurements.reverse()} />
          </Section>
        </Body>
      )}
    </Body>
  );
}

export default observer(QuantityDetails);
