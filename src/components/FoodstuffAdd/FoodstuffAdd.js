import React, { Component } from "react";
import styled from "styled-components";
import styles from "../../styles/constants";
import { decorate, observable, action } from "mobx";
import { inject, observer } from "mobx-react";

import Flex from "../Flex";
import Modal from "../Modal";
import Input from "../Input";
import Select from "../Select";
import Button from "../Button";
import ActionButton from "../ActionButton";
import Container from "../Container";

class FoodstuffAdd extends Component {
  constructor(props) {
    super(props);

    this.values = {
      name: "",
      quantity: "",
      unit: "",
      calories: "",
      carbs: "",
      proteins: "",
      fats: "",
      sugars: "",
      salt: "",
      saturates: ""
    };

    this.invalid = {
      name: false,
      quantity: false,
      unit: false,
      calories: false,
      carbs: false,
      proteins: false,
      fats: false,
      sugars: false,
      salt: false,
      saturates: false
    };
  }

  render() {
    return (
      <Modal>
        <Div>
          <Row>
            <Input
              onChange={this.change}
              value={this.values.name}
              invalid={this.invalid.name}
              name="name"
              placeholder={this.props.t9n.t.name}
            />
          </Row>

          <Row>
            <Input
              onChange={this.change}
              value={this.values.quantity}
              invalid={this.invalid.quantity}
              name="quantity"
              placeholder={this.props.t9n.t.quantity}
            />

            <Select
              onChange={this.change}
              value={this.values.unit}
              invalid={this.invalid.unit}
              name="unit"
              placeholder={this.props.t9n.t.unit}
              options={[
                { value: "g", name: this.props.t9n.t.g },
                { value: "pc", name: this.props.t9n.t.pc },
                { value: "ml", name: this.props.t9n.t.ml }
              ]}
            />
          </Row>

          {[
            "calories",
            "fats",
            "saturates",
            "carbs",
            "sugars",
            "proteins",
            "salt"
          ].map(value => (
            <Row key={value}>
              <Input
                onChange={this.change}
                value={this.values[value]}
                invalid={this.invalid[value]}
                name={value}
                placeholder={this.props.t9n.t.label[value]}
              />
            </Row>
          ))}

          <Row>
            <ActionButton onClick={this.submit}>
              {this.props.t9n.t.add}
            </ActionButton>

            <Button
              onClick={() => {
                this.props.close();
                this.clear();
              }}
            >
              {this.props.t9n.t.cancel}
            </Button>
          </Row>
        </Div>
      </Modal>
    );
  }

  change = event => {
    const { name, value } = event.target;

    this.values[name] = value;
  };

  clear = event => {
    for (const key in this.values) {
      this.values[key] = "";
      this.invalid[key] = false;
    }
  };

  setValid = (names, valid) => {
    if (!(names instanceof Array)) {
      names = [names];
    }

    for (const key in this.invalid) {
      this.invalid[key] = false;
    }

    for (let i = 0; i < names.length; ++i) {
      this.invalid[names[i]] = !valid;
    }
  };

  submit = () => {
    this.props.auth.foodstuffs
      .add(this.values)
      .then(data => {
        this.props.close();
        this.props.use(data);
      })
      .catch(error => {
        if (error.code === 422) {
          this.setValid(error.details.invalid, false);
        } else {
          console.error(error);
        }
      });
  };
}

decorate(FoodstuffAdd, {
  values: observable,
  invalid: observable,
  setValid: action,
  change: action
});

const Div = styled(Container)`
  width: 35rem;
  margin: auto;
`;

const Row = styled(Flex)`
  border-bottom: solid 1px ${styles.borderColor.gray};

  &:last-child {
    border-bottom: 0;
  }
`;

export default inject("auth", "t9n")(observer(FoodstuffAdd));
