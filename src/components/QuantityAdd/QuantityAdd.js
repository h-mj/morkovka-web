import React, { Component } from "react";
import styled from "styled-components";
import { action, decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";

import Modal from "../Modal";
import ModalHeader from "../ModalHeader";
import Section from "../Section";
import Input from "../Input";
import ActionButton from "../ActionButton";
import Container from "../Container";

class QuantityAdd extends Component {
  constructor(props) {
    super(props);

    this.name = "";
    this.unit = "";

    this.invalid = {
      name: false,
      unit: false
    };
  }

  render() {
    return (
      <Modal>
        <ModalHeader
          title={this.props.t9n.t.quantityAdd}
          close={this.props.close}
        />

        <Section>
          <Form onSubmit={this.add} noValidate>
            <Input
              placeholder={this.props.t9n.t.name}
              name="name"
              value={this.name}
              invalid={this.invalid.name}
              onChange={this.change}
              autoFocus
            />

            <Column>
              <Input
                placeholder={this.props.t9n.t.unit}
                name="unit"
                value={this.unit}
                invalid={this.invalid.unit}
                onChange={this.change}
              />
            </Column>

            <Column>
              <ActionButton onClick={this.add}>
                {this.props.t9n.t.add}
              </ActionButton>
            </Column>
          </Form>
        </Section>
      </Modal>
    );
  }

  setValid = (names, valid) => {
    if (!(names instanceof Array)) {
      names = [names];
    }

    for (const key in this.invalid) {
      this.invalid[key] = false;
    }

    for (let i = 0; i < names.length; ++i) {
      this.invalid[names[i]] = !valid;
    }
  };

  add = event => {
    event.preventDefault();

    this.props.auth.user.quantities
      .add(this.name, this.unit)
      .then(quantity => {
        this.props.close();
        this.props.showDetails(quantity);
      })
      .catch(error => {
        if (error.code === 422) {
          this.setValid(error.details.invalid, false);
        } else {
          console.log(error);
        }
      });
  };

  change = event => {
    const { name, value } = event.target;
    this[name] = value;
  };
}

decorate(QuantityAdd, {
  name: observable,
  unit: observable,
  invalid: observable,
  setValid: action,
  change: action,
  add: action
});

const Form = styled(Container)`
  display: flex;
`;

const Column = styled.div`
  width: 10rem;
  height: 100%;
`;

export default inject("auth", "t9n")(observer(QuantityAdd));
