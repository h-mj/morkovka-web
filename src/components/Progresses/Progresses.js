import React from "react";
import styled from "styled-components";
import { inject, observer } from "mobx-react";
import totals from "../../utils/totals";

import Section from "../Section";
import Subsection from "../Subsection";
import Progress from "./components/Progress";

function Progresses({ t9n, day, past }) {
  const { meals, ratios, measurements } = day;

  if (!ratios || !measurements) {
    return null;
  }

  const total = totals(day.user, day);

  return (
    <Section>
      {[["calories", "carbs"], ["proteins", "fats"]].map((row, index) => (
        <Row key={index.toString()}>
          {row.map(column => (
            <Progress
              key={column}
              title={t9n.t[column]}
              progress={meals[column]}
              total={total[column]}
              calories={column === "calories"}
              past={past}
            />
          ))}
        </Row>
      ))}
    </Section>
  );
}

const Row = styled(Subsection)`
  display: flex;

  @media screen and (max-width: 40rem) {
    flex-direction: column;

    & > * {
      margin-bottom: 1rem;
    }

    & > *:last-child {
      margin-bottom: 0;
    }
  }
`;

export default inject("t9n")(observer(Progresses));
