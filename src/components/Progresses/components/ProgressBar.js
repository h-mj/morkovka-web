import React from "react";
import styled from "styled-components";
import styles from "../../../styles/constants";

export default function ProgressBar({ percentage, over }) {
  return (
    <Container over={over}>
      <Progress over={over} percentage={percentage} />
    </Container>
  );
}

const Container = styled.div`
  width: 100%;
  height: 4px;
  background-color: ${props =>
    (props.over && styles.backgroundColor.primary) ||
    styles.backgroundColor.slate};
`;

const Progress = styled.div`
  width: ${props => props.percentage + "%"};
  height: 100%;
  background-color: ${props =>
    (props.over && "red") || styles.backgroundColor.primary};
`;
