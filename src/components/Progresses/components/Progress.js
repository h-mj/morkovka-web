import React from "react";
import styled from "styled-components";
import styles from "../../../styles/constants";
import { inject, observer } from "mobx-react";

import ProgressBar from "./ProgressBar";
import Container from "../../Container";

function Progress({ t9n, title, progress, total, calories, past }) {
  let difference, message, over;
  let percentage = progress / total * 100;

  if (progress < total) {
    difference = Math.round(total) - Math.round(progress);
    message = past ? t9n.t.progress.under : t9n.t.progress.left;
    over = false;
  } else {
    difference = Math.round(progress) - Math.round(total);
    message = t9n.t.progress.over;
    percentage = Math.min(percentage - 100, 100);
    over = true;
  }

  let unit = t9n.t.g;

  if (calories) {
    unit = "";
  }

  return (
    <Div>
      <ProgressBar percentage={percentage} over={over} />
      <Header>
        <Title>{title}</Title>
        <Status>
          ({Math.round(progress)}
          {unit}/{Math.round(total)}
          {unit})
        </Status>
        <Left>
          <strong>
            {difference}
            {unit}
          </strong>{" "}
          {message}
        </Left>
      </Header>
    </Div>
  );
}

const Div = styled(Container)`
  margin-right: 1rem;

  &:last-child {
    margin-right: 0;
  }
`;

const Header = styled.div`
  display: flex;
  padding: 0 1rem;
  align-items: center;
  justify-content: center;
  line-height: 3.5rem;
  color: ${styles.color.text};
`;

const Title = styled.div`
  font-weight: bold;
`;

const Status = styled.div`
  margin-left: 0.5rem;
  font-size: 0.9rem;
  width: 100%;
`;

const Left = styled.div`
  white-space: nowrap;
`;

export default inject("t9n")(observer(Progress));
