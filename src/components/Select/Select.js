import React from "react";
import styled from "styled-components";
import styles from "../../styles/constants";

export default function Select({ options = [], value, placeholder, ...rest }) {
  return (
    <ActualSelect required {...rest} value={value ? value : placeholder && ""}>
      {placeholder && (
        <Option value="" disabled hidden>
          {placeholder}
        </Option>
      )}

      {options.map(({ value, name }) => (
        <Option value={value} key={value}>
          {name}
        </Option>
      ))}
    </ActualSelect>
  );
}

const ActualSelect = styled.select`
  width: 100%;

  border: none;
  outline: none;
  margin: 0;
  padding: 0;
  background: none;

  color: ${styles.color.text};
  padding: 1rem 1rem 1rem 0.75rem;
  line-height: 1.5rem;
  box-sizing: border-box;
  background-color: white;

  ${props => props.invalid && "background-color: #ffdddd"};

  &:invalid {
    color: ${styles.color.textLight};
  }
`;

const Option = styled.option`
  color: ${styles.color.text};
`;
