import React from "react";
import { Bar } from "react-chartjs-2";

import ChartContainer from "../ChartContainer";

export default function ConsumptionChart({ of, label, data, onElementsClick }) {
  const labels = data.map(({ date }) => date);
  const consumed = data.map(({ consumed }) => Math.round(consumed[of]));

  const totals = data.map(({ totals }) =>
    totals ? Math.round(totals[of]) : null
  );

  const backgroundColors = [];
  const borderColors = [];

  for (let i = 0; i < totals.length; ++i) {
    let limit = totals[i];
    let actual = consumed[i];

    if (limit === null) {
      backgroundColors.push("#f5f5f5");
      borderColors.push("#333333");
    } else if (Math.abs(limit - actual) / limit <= 0.15) {
      backgroundColors.push("#92e492");
      borderColors.push("#279f27");
    } else {
      backgroundColors.push("#ff9784");
      borderColors.push("#fa2600");
    }
  }

  const chartData = {
    labels,
    datasets: [
      {
        type: "line",
        label,
        data: totals,
        fill: false,
        borderColor: "#279f27",
        pointBackgroundColor: "#279f27"
      },
      {
        type: "bar",
        label,
        data: consumed,
        backgroundColor: backgroundColors,
        borderColor: borderColors,
        borderWidth: 1
      }
    ]
  };

  const chartOptions = {
    elements: {
      point: {
        radius: 0
      }
    },
    legend: {
      display: false
    },
    layout: {
      padding: {
        top: 5
      }
    },
    scales: {
      xAxes: [
        {
          ticks: {
            maxRotation: 0,
            minRotation: 0,
            autoSkip: true
          },
          gridLines: {
            display: false
          },
          offset: true,
          type: "time",
          time: {
            unit: "day",
            stepSize: 1,
            displayFormats: {
              day: "DD"
            },
            tooltipFormat: "DD.MM.YYYY"
          }
        }
      ],
      yAxes: [
        {
          display: false,
          gridLines: {
            display: false
          },
          ticks: {
            beginAtZero: true
          }
        }
      ]
    }
  };

  return (
    <ChartContainer>
      <Bar
        height={100}
        data={chartData}
        options={chartOptions}
        onElementsClick={onElementsClick}
      />
    </ChartContainer>
  );
}
