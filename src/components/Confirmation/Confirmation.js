import React from "react";
import styled from "styled-components";
import styles from "./../../styles/constants";
import { inject, observer } from "mobx-react";

import Modal from "../Modal";
import Button from "../Button";
import ActionButton from "../ActionButton";
import Container from "../Container";

function Confirmation({ t9n, message, onYes, onNo }) {
  return (
    <Modal>
      <Div>
        <MessageDiv>
          <Message>{message}</Message>
        </MessageDiv>

        <Buttons>
          <Button onClick={onYes}>{t9n.t.confirm.yes}</Button>
          <ActionButton onClick={onNo}>{t9n.t.confirm.no}</ActionButton>
        </Buttons>
      </Div>
    </Modal>
  );
}

const Div = styled(Container)`
  width: 30rem;
  margin: 4rem auto;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const MessageDiv = styled.div`
  width: 100%;
  height: 8rem;
  padding: 0 4rem;
  line-height: 1.5rem;
  box-sizing: border-box;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Message = styled.div`
  font-size: 1.1rem;
  color: ${styles.color.text};
`;

const Buttons = styled.div`
  display: flex;
  flex-shrink: 0;
  width: 100%;
`;

export default inject("t9n")(observer(Confirmation));
