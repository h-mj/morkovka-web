import styled from "styled-components";
import styles from "../../styles/constants";

export default styled.div`
  width: 100%;
  font-size: 1.2rem;
  margin-bottom: 1rem;
  color: ${styles.color.text};
`;
