import * as React from "react";
import styled from "styled-components";
import { Redirect } from "react-router-dom";
import { inject, observer } from "mobx-react";

const Auth = ({ auth, children }: any) => {
  if (auth.authenticated) {
    if (auth.user) {
      return <Div>{children}</Div>;
    }

    return null;
  }

  auth.logout();

  return <Redirect to="/" />;
};

const Div = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export default inject("auth")(observer(Auth));
