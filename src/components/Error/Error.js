import styled from "styled-components";

export default styled.div`
  color: red;
  line-height: 1.5rem;
  margin-bottom: 1rem;
  text-align: center;
`;
