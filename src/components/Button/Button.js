import styled from "styled-components";
import styles from "../../styles/constants";

export default styled.button`
  width: 100%;

  border: none;
  outline: none;
  margin: 0;
  padding: 0;
  background: none;

  padding: 1rem 0;
  line-height: 1.5rem;
  box-sizing: border-box;
  background-color: ${styles.backgroundColor.slate};
  color: ${styles.color.text};

  cursor: pointer;
  transition: 0.2s;

  :hover {
    background-color: ${styles.backgroundColor.slateAction};
  }
`;
