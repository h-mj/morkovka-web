import React from "react";
import { Line } from "react-chartjs-2";
import styles from "../../styles/constants";
import { inject, observer } from "mobx-react";
import * as ChartDataLabels from "chartjs-plugin-datalabels";

import ChartContainer from "../ChartContainer";

import Quantity from "../../models/Quantity";

function MeasurementChart({ t9n, quantity }) {
  const values = quantity.measurements.map(({ date, value }) => ({
    t: date,
    y: value
  }));

  const data = {
    datasets: [
      {
        label: `${quantity.prettyName} (${quantity.prettyUnit})`,
        data: values,
        pointBackgroundColor: styles.backgroundColor.primaryAction,
        borderColor: styles.backgroundColor.primary,
        backgroundColor: styles.backgroundColor.primaryLight
      }
    ]
  };

  const options = {
    layout: {
      padding: {
        top: 25,
      }
    },
    legend: {
      display: false
    },
    plugins: {
      datalabels: {
        align: "top",
        formatter: value => {
          return quantity.type === Quantity.ACTIVENESS
            ? t9n.t.activeness[Quantity.ACTIVENESS_NAMES[value.y]]
            : value.y;
        }
      }
    },
    tooltips: {
      enabled: true,
      mode: "single",
      callbacks: {
        label: function(tooltipItems, data) {
          return (
            data.datasets[tooltipItems.datasetIndex].label +
            ": " +
            (quantity.type === Quantity.ACTIVENESS
              ? t9n.t.activeness[Quantity.ACTIVENESS_NAMES[tooltipItems.yLabel]]
              : tooltipItems.yLabel)
          );
        }
      }
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false
          },
          offset: true,
          type: "time",
          time: {
            unit: "day",
            stepSize: 1,
            displayFormats: {
              day: "DD.MM.YYYY"
            },
            tooltipFormat: "DD.MM.YYYY"
          },
          ticks: {
            maxRotation: 0,
            minRotation: 0,
            autoSkip: true,
            maxTicksLimit: 7
          }
        }
      ],
      yAxes: [
        {
          gridLines: {
            display: false
          },
          ticks: {
            autoSkip: true,
            maxTicksLimit: 5,
            callback: value =>
              quantity.type === Quantity.ACTIVENESS
                ? t9n.t.activeness[Quantity.ACTIVENESS_NAMES[value]]
                : value
          }
        }
      ]
    }
  };

  return (
    <ChartContainer>
      <Line
        height={100}
        data={data}
        options={options}
        plugins={[ChartDataLabels]}
      />
    </ChartContainer>
  );
}

export default inject("t9n")(observer(MeasurementChart));
