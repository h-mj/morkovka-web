import React from "react";

import Section from "../Section";
import SectionTitle from "../SectionTitle";
import MeasurementChart from "../MeasurementChart";

export default function MeasurementHistory({ quantities }) {
  return (
    <Section>
      {quantities.quantities.map(
        quantity =>
          (quantity.type === 1 || quantity.type > 3) &&
          quantity.value !== null && (
            <Section key={quantity.id.toString()}>
              <SectionTitle>{quantity.prettyName}</SectionTitle>
              <MeasurementChart quantity={quantity} />
            </Section>
          )
      )}
    </Section>
  );
}
