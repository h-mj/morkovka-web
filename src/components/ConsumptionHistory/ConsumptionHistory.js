import React, { Component } from "react";
import { action, decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";

import Section from "../Section";
import SectionTitle from "../SectionTitle";
import ConsumptionChart from "../ConsumptionChart";
import ConsumptionData from "../ConsumptionData/ConsumptionData";

class ConsumptionHistory extends Component {
  render() {
    const { t9n, data, user } = this.props;

    return (
      <Section>
        {["calories", "carbs", "proteins", "fats"].map(value => (
          <Section key={value}>
            <SectionTitle>{t9n.t.title[value]}</SectionTitle>
            <ConsumptionChart
              of={value}
              label={t9n.t.label[value]}
              data={data}
              onElementsClick={this.onElementsClick}
            />
          </Section>
        ))}

        {this.date && <ConsumptionData user={user} close={this.hideData} />}
      </Section>
    );
  }

  onElementsClick = elements => {
    if (elements.length === 0) {
      return;
    }

    const data = this.props.data[elements[0]._index];

    if (data.consumed.calories > 0) {
      this.showData(data.date);
    }
  };

  showData = date => {
    this.date = date;

    const user = this.props.user;

    if (!user.day || user.day.date !== date) {
      user.loadDay(this.date);
    }
  };

  hideData = () => {
    this.date = null;
  };
}

decorate(ConsumptionHistory, {
  date: observable,
  showData: action,
  hideData: action
});

export default inject("t9n")(observer(ConsumptionHistory));
