import React, { Component } from "react";
import styled from "styled-components";
import styles from "../../styles/constants";
import { action, decorate, observable } from "mobx";
import { observer } from "mobx-react";

import Confirmation from "../Confirmation";
import ConfirmationWithPassword from "../ConfirmationWithPassword";

class Remove extends Component {
  constructor(props) {
    super(props);

    this.confirmation = false;
  }

  render() {
    const { remove, message, withPassword, children } = this.props;

    return (
      <div>
        {(children && (
          <Button onClick={this.showConfirmation}>{children}</Button>
        )) || <Button onClick={this.showConfirmation}>&#10006;</Button>}

        {this.confirmation &&
          (withPassword ? (
            <ConfirmationWithPassword
              message={message}
              onYes={remove}
              onNo={this.hideConfirmation}
            />
          ) : (
            <Confirmation
              message={message}
              onYes={remove}
              onNo={this.hideConfirmation}
            />
          ))}
      </div>
    );
  }

  showConfirmation = event => {
    this.confirmation = true;
    event.stopPropagation();
  };

  hideConfirmation = () => {
    this.confirmation = false;
  };
}

const Button = styled.div`
  cursor: pointer;
  user-select: none;
  color: ${styles.color.text};
  transition: 0.2s;

  &:hover {
    color: red;
  }
`;

decorate(Remove, {
  confirmation: observable,
  showConfirmation: action,
  hideConfirmation: action
});

export default observer(Remove);
