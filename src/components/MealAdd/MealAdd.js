import React, { Component } from "react";
import styled from "styled-components";
import styles from "../../styles/constants";

import ActionButton from "../ActionButton";
import { observer, inject } from "mobx-react";

class MealAdd extends Component {
  render() {
    const { t9n, meals } = this.props;

    return (
      <Container>
        {["breakfast", "lunch", "dinner", "snack"].map(
          value =>
            !meals.existsByName(value) && (
              <AddButton key={value} name={value} onClick={this.add}>
                {t9n.t.addMeal[value]}
              </AddButton>
            )
        )}
      </Container>
    );
  }

  add = event => {
    this.props.meals.add(event.target.name);
  };
}

const Container = styled.div`
  display: flex;
  transition: 0.2s;
`;

const AddButton = styled(ActionButton)`
  box-shadow: ${styles.boxShadow._1};
  margin-right: 1rem;
  min-width: 0;

  &:last-child {
    margin-right: 0;
  }

  @media screen and (max-width: 40rem) {
    white-space: pre-wrap;
    line-height: 1.15rem;
  }
`;

export default inject("t9n")(observer(MealAdd));
