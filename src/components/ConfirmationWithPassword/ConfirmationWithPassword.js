import React, { Component } from "react";
import styled from "styled-components";
import styles from "./../../styles/constants";
import { action, decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";

import Modal from "../Modal";
import Input from "../Input";
import Separator from "../Separator";
import Button from "../Button";
import ActionButton from "../ActionButton";
import Container from "../Container";

class ConfirmationWithPassword extends Component {
  constructor(props) {
    super(props);

    this.password = "";
    this.invalid = false;
  }

  render() {
    const { t9n, message, onNo } = this.props;

    return (
      <Modal>
        <Div>
          <MessageDiv>
            <Message>{message}</Message>
          </MessageDiv>

          <Separator />

          <Input
            onChange={this.change}
            value={this.password}
            invalid={this.invalid}
            name="password"
            type="password"
            placeholder={t9n.t.registration.password}
          />

          <Buttons>
            <Button onClick={this.submit}>{t9n.t.confirm.yes}</Button>
            <ActionButton onClick={onNo}>{t9n.t.confirm.no}</ActionButton>
          </Buttons>
        </Div>
      </Modal>
    );
  }

  change = event => {
    this.password = event.target.value;
    this.invalid = false;
  };

  submit = () => {
    if (!this.password) {
      this.invalid = true;
      return;
    }

    this.props.onYes(this.password).catch(error => {
      this.invalid = true;
    });
  };
}

decorate(ConfirmationWithPassword, {
  password: observable,
  invalid: observable,
  change: action,
  submit: action
});

const Div = styled(Container)`
  width: 30rem;
  margin: 4rem auto;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  cursor: default;
`;

const MessageDiv = styled.div`
  width: 100%;
  height: 8rem;
  padding: 0 4rem;
  line-height: 1.5rem;
  box-sizing: border-box;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Message = styled.div`
  font-size: 1.1rem;
  color: ${styles.color.text};
`;

const Buttons = styled.div`
  display: flex;
  flex-shrink: 0;
  width: 100%;
`;

export default inject("t9n")(observer(ConfirmationWithPassword));
