import React, { Component } from "react";
import styled from "styled-components";
import { inject, observer } from "mobx-react";

import ActionButton from "../ActionButton";
import Client from "./components/Client";
import CodeGenerator from "./components/CodeGenerator";
import { decorate, observable, action } from "mobx";

class ClientList extends Component {
  constructor(props) {
    super(props);

    this.visible = false;
    this.code = null;
  }

  render() {
    const { t9n, clients, select, client: current } = this.props;

    return (
      <Container>
        <ActionButton onClick={this.show}>{t9n.t.add}</ActionButton>

        {this.visible && <CodeGenerator close={this.hide} code={this.code} />}

        {clients.clients.map(client => (
          <Client
            key={client.id}
            client={client}
            select={select}
            current={client === current}
          />
        ))}
      </Container>
    );
  }

  show = () => {
    this.visible = true;

    this.props.clients.generateLink().then(code => {
      this.code = code;
    });
  };

  hide = () => {
    this.visible = false;
    this.code = null;
  };
}

decorate(ClientList, {
  visible: observable,
  code: observable,
  show: action,
  hide: action
});

const Container = styled.div`
  width: 25rem;
  height: 100%;
  overflow-y: auto;
  flex-shrink: 0;
  background-color: white;
`;

export default inject("t9n")(observer(ClientList));
