import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import Modal from "../../Modal";
import ModalHeader from "../../ModalHeader";
import Body from "../../Section";
import Container from "../../Container";
import Flex from "../../Flex";
import Input from "../../Input";

class CodeGenerator extends Component {
  render() {
    return (
      <Modal>
        <ModalHeader
          title={this.props.t9n.t.generatedCode}
          close={this.props.close}
        />

        {this.props.code && (
          <Body>
            <Container>
              <Flex>
                <Input
                  value={this.props.code}
                  innerRef={input => input && input.select()}
                  readOnly
                />
              </Flex>
            </Container>
          </Body>
        )}
      </Modal>
    );
  }

  copy = () => {};
}

export default inject("t9n")(observer(CodeGenerator));
