import React from "react";
import styled from "styled-components";
import styles from "../../../styles/constants";
import { inject, observer } from "mobx-react";
import moment from "moment";

import Flex from "../../Flex";
import Remove from "../../Remove";

function Client({ t9n, client, select, current }) {
  const sex =
    client.sex === "f"
      ? t9n.t.user.female.charAt(0)
      : t9n.t.user.male.charAt(0);

  const age = moment().diff(moment(client.date_of_birth), "years");

  return (
    <Div onClick={() => select(client)} current={current}>
      <Name>{client.name}</Name>

      <Attributes>
        <Attribute color={client.sex === "f" ? "#ff3366" : "#0055aa"}>
          <Key>{t9n.t.user.sex}</Key>
          {sex}
        </Attribute>

        <Attribute>
          <Key>{t9n.t.user.age}</Key>
          {age}
        </Attribute>
      </Attributes>

      <Remove
        message={t9n.t.confirm.client}
        remove={password => this.props.client.remove(password)}
        withPassword
      />
    </Div>
  );
}

const Div = styled(Flex)`
  padding: 0 1rem;
  height: 3.5rem;
  box-sizing: border-box;
  align-items: center;
  color: ${styles.color.text};
  border-bottom: solid 1px ${styles.borderColor.gray};

  user-select: none;
  cursor: pointer;
  transition: 0.2s;

  &:last-child {
    border-bottom: 0;
  }

  &:hover {
    background-color: #f5f5f5;

    ${props =>
      props.current &&
      `background-color: ${styles.backgroundColor.primaryLight}`};
  }

  ${props =>
    props.current &&
    `background-color: ${styles.backgroundColor.primaryLight}`};
`;

const Name = styled.div`
  width: 100%;
  line-height: 1rem;
  box-sizing: border-box;
`;

const Attributes = styled.div`
  display: flex;
  margin-right: 1rem;
`;

const Attribute = styled.div`
  width: 2.5rem;
  display: flex;
  align-items: center;
  flex-direction: column;
  margin-right: 1rem;

  ${props => props.color && `color: ${props.color}`};

  &:last-child {
    margin-right: 0;
  }
`;

const Key = styled.div`
  font-size: 0.6rem;
  color: ${styles.color.textLight};
`;

export default inject("t9n")(observer(Client));
