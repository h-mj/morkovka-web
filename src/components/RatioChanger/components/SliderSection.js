import React from "react";
import ReactSlider from "react-slider";
import styled from "styled-components";
import { inject, observer } from "mobx-react";
import round from "../../../utils/round";

import Container from "../../Container";
import styles from "../../../styles/constants";
import Flex from "../../Flex";

function SliderSection({ t9n, ratios, totals, measurements, ...rest }) {
  return (
    <Div>
      <Slider value={[ratios.carbs, ratios.carbs + ratios.proteins]} {...rest}>
        <Handle />
        <Handle />
      </Slider>

      <Sections>
        {["carbs", "proteins", "fats"].map(value => (
          <Section
            key={value}
            title={t9n.t[value]}
            percentage={ratios[value]}
            total={totals ? totals[value] : null}
            mass={measurements ? measurements.mass : null}
            t9n={t9n}
          />
        ))}
      </Sections>
    </Div>
  );
}

const Div = styled(Container)`
  height: 100%;
  flex-shrink: 1;
  display: flex;
  flex-direction: column;
`;

const Slider = styled(ReactSlider)`
  height: 2rem;
  flex-shrink: 0;
  border-bottom: solid 1px ${styles.borderColor.gray};
`;

const Sections = styled(Flex)`
  width: 100%;
  height: 100%;
`;

function Section({ t9n, title, percentage, total, mass }) {
  return (
    <SectionDiv>
      <Title>{title}</Title>
      <Percentage>{percentage}%</Percentage>
      {total !== null && (
        <Total>
          {Math.round(total)}
          {t9n.t.g}, {round(total / mass, 2)}
          {t9n.t.g}/{t9n.t.kg}
        </Total>
      )}
    </SectionDiv>
  );
}

const SectionDiv = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Title = styled.div`
  font-size: 0.9rem;
  white-space: nowrap;
  color: ${styles.color.text};
`;

const Percentage = styled.div`
  margin: 0.25rem 0;
  font-size: 1.5rem;
  color: ${styles.color.text};
`;

const Total = styled.div`
  font-size: 0.9rem;
  color: ${styles.color.textLight};
  white-space: nowrap;
  min-width: 0;
`;

const Handle = styled.div`
  width: 0.5rem;
  height: 2rem;
  cursor: ew-resize;
  background-color: ${styles.backgroundColor.primary};
`;

export default inject("t9n")(observer(SliderSection));
