import React from "react";
import styled from "styled-components";
import styles from "../../../styles/constants";

import Container from "../../Container";
import Input from "../../Input";

export default function InputSection({ title, ...props }) {
  return (
    <Div>
      <Title>{title}</Title>
      <ActualInput {...props} />
    </Div>
  );
}

const Div = styled(Container)`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const Title = styled.div`
  height: 2rem;
  line-height: 2rem;
  font-size: 0.9rem;
  text-align: center;
  flex-shrink: 0;
  color: ${styles.color.text};
  border-bottom: solid 1px ${styles.borderColor.gray};
  overflow: hidden;
  text-overflow: ellipsis;
`;

const ActualInput = styled(Input)`
  text-align: center;
  height: 100%;
  font-size: 1.5rem;
  position: absolute;
`;
