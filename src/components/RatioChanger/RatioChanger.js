import React, { Component } from "react";
import styled from "styled-components";
import { action, decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import getTotals from "../../utils/totals";
import styles from "../../styles/constants";

import Section from "../Section";
import InputSection from "./components/InputSection";
import SliderSection from "./components/SliderSection";

class RatioChanger extends Component {
  constructor(props) {
    super(props);

    let { date, measurements, ratios, totals } = this.props.user;

    this.ratios = ratios
      ? { ...ratios }
      : { delta: 0, carbs: 40, proteins: 30, fats: 30 };

    if (!totals && measurements) {
      totals = getTotals(this.props.user, {
        date,
        measurements,
        ratios: this.ratios
      });
    }

    if (totals) {
      this.totals = { ...totals };
      this.totals.calories = Math.round(this.totals.calories);
    }
  }

  render() {
    return (
      <FlexSection>
        {this.totals && (
          <Column>
            <InputSection
              title={this.props.t9n.t.ratioChanger.calories}
              value={this.totals.calories}
              onChange={this.onCalorieChange}
            />
          </Column>
        )}

        <Column>
          <InputSection
            title={this.props.t9n.t.ratioChanger.change}
            value={this.ratios.delta}
            onChange={this.onDeltaChange}
          />
        </Column>

        <FillColumn>
          <SliderSection
            ratios={this.ratios}
            totals={this.totals}
            measurements={this.props.user.measurements}
            onChange={this.onSliderChange}
            onAfterChange={this.updateRatios}
          />
        </FillColumn>
      </FlexSection>
    );
  }

  onCalorieChange = event => {
    const value = event.target.value;
    const intValue = parseInt(value, 10);

    if (!isNaN(intValue)) {
      const defaultTotal = this.totals.defaultCalories;

      this.totals.calories = intValue;
      this.ratios.delta = Math.round((intValue / defaultTotal - 1) * 100);

      this.updateRatios("calories");
    } else {
      this.totals.calories = value;
    }
  };

  onDeltaChange = event => {
    const value = event.target.value;
    const intValue = parseInt(value, 10);

    if (!isNaN(intValue)) {
      this.ratios.delta = intValue;
      this.updateRatios("delta");
    } else {
      this.ratios.delta = value;
    }
  };

  onSliderChange = values => {
    const [first, second] = values;

    this.ratios.carbs = first;
    this.ratios.proteins = second - first;
    this.ratios.fats = 100 - second;

    if (this.totals) {
      const { date, measurements } = this.props.user;

      this.totals = getTotals(this.props.user, {
        date,
        measurements,
        ratios: this.ratios
      });

      this.totals.calories = Math.round(this.totals.calories);
    }
  };

  updateRatios = (by = null) => {
    this.props.user.addRatios(this.ratios).then(ratios => {
      if (by === "delta") {
        const delta = this.ratios.delta;

        this.props.user.ratios = ratios;
        this.ratios = ratios;
        this.ratios.delta = delta;
      } else {
        this.props.user.ratios = ratios;
        this.ratios = ratios;
      }

      if (this.props.onChange) {
        this.props.onChange(ratios);
      }

      const totals = this.props.user.totals;

      if (!totals) {
        return;
      }

      if (by === "calories") {
        const calories = this.totals.calories;

        this.totals = totals;
        this.totals.calories = calories;
      } else {
        this.totals = totals;
        this.totals.calories = Math.round(this.totals.calories);
      }
    });
  };
}

decorate(RatioChanger, {
  ratios: observable,
  totals: observable,
  onCalorieChange: action,
  onDeltaChange: action,
  onSliderChange: action,
  updateRatios: action
});

const FlexSection = styled(Section)`
  display: flex;
`;

const Column = styled.div`
  width: 20%;
  height: 8rem;
  margin-right: 1rem;
  flex-shrink: 0;

  &:last-child {
    margin-right: 0;
  }

  @media screen and (max-width: 40rem) {
    margin-right: 0;
    border-right: solid 1px ${styles.borderColor.gray};

    &:last-child {
      border-right: 0;
    }
  }
`;

const FillColumn = styled(Column)`
  width: 100%;
  flex-shrink: 1;
`;

export default inject("t9n")(observer(RatioChanger));
