import React from "react";
import styled from "styled-components";
import round from "../../utils/round";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";

import Flex from "../Flex";
import Section from "../Section";
import SectionTitle from "../SectionTitle";
import Container from "../Container";
import styles from "../../styles/constants";

const bodyMassIndexRanges = [
  { class: 0, min: -Infinity, lt: 16 },
  { class: 1, min: 16, lt: 18.5 },
  { class: 2, min: 18.5, lt: 25 },
  { class: 3, min: 25, lt: 30 },
  { class: 4, min: 30, lt: 35 },
  { class: 5, min: 35, lt: 40 },
  { class: 6, min: 40, lt: Infinity }
];

function getRangeIndex(bodyMassIndex) {
  for (let i = 0; i < bodyMassIndexRanges.length; ++i) {
    const range = bodyMassIndexRanges[i];

    if (range.min <= bodyMassIndex && bodyMassIndex < range.lt) {
      return i;
    }
  }
}

function BodyMassIndexScale({ t9n, user }) {
  const { measurements } = user;

  if (!measurements) {
    return (
      <Section>
        <SectionTitle>{t9n.t.title.bmi}</SectionTitle>
        <Div>
          <A to="/quantities">
            {t9n.t.bmi.noData}
          </A>
        </Div>
      </Section>
    );
  }

  const { mass, height } = measurements;
  const heightSquared = height * height / 100 / 100;
  const bmi = mass / heightSquared;
  const index = getRangeIndex(bmi);
  const range = bodyMassIndexRanges[index];
  const lowerRange = bodyMassIndexRanges[index - 1];
  const upperRange = bodyMassIndexRanges[index + 1];
  const lowerMass = round(range.min * heightSquared, 1);
  const upperMass = round(range.lt * heightSquared, 1);

  let percentage;

  if (lowerRange && upperRange) {
    percentage = (mass - lowerMass) / (upperMass - lowerMass) * 100;
  } else {
    percentage = lowerRange ? 10 : 90;
  }

  return (
    <Section>
      <SectionTitle>{t9n.t.title.bmi}</SectionTitle>
      <Div>
        {lowerRange && <Side>{t9n.t.bmiRanges[lowerRange.class]}</Side>}
        {lowerRange && <Post value={lowerMass} />}
        <Background>
          {t9n.t.bmiRanges[range.class]}
          <Post value={mass} primary percentage={percentage} />
        </Background>
        {upperRange && <Post value={upperMass} />}
        {upperRange && <Side>{t9n.t.bmiRanges[upperRange.class]}</Side>}
      </Div>
    </Section>
  );
}

const Div = styled(Container)`
  display: flex;
  height: 3.5rem;
  align-items: center;
  justify-content: center;
  white-space: pre-wrap;
  text-align: center;
`;

const A = styled(Link)`
  color: red;
  text-decoration: underline;
`;

const Background = styled(Flex)`
  height: 100%;
  position: relative;
  align-items: center;
  justify-content: center;
  color: ${styles.color.text};
`;

const Side = styled(Background)`
  width: 25%;
  flex-shrink: 0;
`;

function Post({ value, primary, percentage }) {
  if (primary) {
    return (
      <ZeroWidthAbsolute percentage={percentage}>
        <PostValue primary>{value}</PostValue>
        <PostBody primary />
      </ZeroWidthAbsolute>
    );
  }

  return (
    <ZeroWidth>
      <PostValue>{value}</PostValue>
      <PostBody />
    </ZeroWidth>
  );
}

const ZeroWidth = styled(Background)`
  width: 0;
  flex-direction: column;
`;

const ZeroWidthAbsolute = styled(ZeroWidth)`
  bottom: 0;
  position: absolute;
  z-index: 1;
  left: ${props => props.percentage + "%"};
`;

const PostValue = styled.div`
  padding: 0.2rem 0.5rem;
  color: ${styles.color.text};
  font-size: 0.9rem;
  white-space: nowrap;

  ${props =>
    props.primary &&
    "background: linear-gradient(90deg, #ffffff00 0%, #ffffff 10%, #ffffff 90%, #ffffff00 100%)"};
`;

const PostBody = styled.div`
  width: ${props => (props.primary && "0.5rem") || "0.25rem"};
  height: 100%;
  opacity: ${props => (props.primary && 1) || 0.6};
  background-color: ${styles.backgroundColor.primary};
`;

export default inject("t9n")(observer(BodyMassIndexScale));
