import styled from "styled-components";
import styles from "../../styles/constants";

export default styled.div`
  width: 100%;
  height: 1px;
  background-color: ${styles.borderColor.gray};
`;
