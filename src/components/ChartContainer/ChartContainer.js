import styled from "styled-components";

import Container from "../Container";

export default styled(Container)`
  padding: 1rem;
  box-sizing: border-box;
`;
