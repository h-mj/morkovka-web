import styled from "styled-components";
import styles from "../../styles/constants";

export default styled.div`
  width: 100%;
  background-color: white;
  box-shadow: ${styles.boxShadow._1};
`;
