import React from "react";
import styled from "styled-components";
import styles from "../../styles/constants";

import Section from "../Section";

export default function ModalHeader({ title, close }) {
  return (
    <Section>
      <Div>
        <Title>{title}</Title>
        <Close onClick={close}>&#10006;</Close>
      </Div>
    </Section>
  );
}

const Div = styled.div`
  display: flex;
  width: 100%;
  height: 3.5rem;
  align-items: center;
  justify-content: space-between;
`;

const Title = styled.div`
  font-size: 1.5rem;
  color: ${styles.color.text};
`;

const Close = styled.div`
  color: ${styles.color.text};
  font-size: 1.5rem;

  user-select: none;
  cursor: pointer;
  transition: 0.2s;

  &:hover {
    color: red;
  }
`;
