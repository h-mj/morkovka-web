import React from "react";
import { observer } from "mobx-react";

import Section from "../Section";
import Container from "../Container";
import Header from "./components/Header";
import Labels from "./components/Labels";
import Food from "./components/Food";
import ExpandButton from "./components/ExpandButton";

function Meal({ meal, addFood, readOnly }) {
  return (
    <Section>
      <Container>
        <Header meal={meal} deletable={!readOnly} />

        {!meal.empty && <Labels deletable={!readOnly} />}

        <div>
          {meal.foods.map(food => (
            <Food food={food} key={food.id.toString()} deletable={!readOnly} />
          ))}
        </div>

        {!readOnly && <ExpandButton onClick={() => addFood(meal)} />}
      </Container>
    </Section>
  );
}

export default observer(Meal);
