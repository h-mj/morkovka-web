import React from "react";
import styled, { css } from "styled-components";
import { inject, observer } from "mobx-react";

import Flex from "../../Flex";

import Column from "./Column";
import FixedColumn from "./FixedColumn";
import ThinColumn from "./ThinColumn";

function Labels({ t9n, deletable }) {
  return (
    <Flex>
      <LabelFixedColumn>
        <span>{t9n.t.quantity}</span>
      </LabelFixedColumn>

      <LabelColumn>
        <span>{t9n.t.name}</span>
      </LabelColumn>

      <LabelFixedColumn>
        <span>{t9n.t.calories}</span>
      </LabelFixedColumn>

      <LabelFixedColumn>
        <span>{t9n.t.carbs}</span>
      </LabelFixedColumn>

      <LabelFixedColumn>
        <span>{t9n.t.proteins}</span>
      </LabelFixedColumn>

      <LabelFixedColumn>
        <span>{t9n.t.fats}</span>
      </LabelFixedColumn>

      {deletable && <LabelThinColumn />}
    </Flex>
  );
}

const style = css`
  font-size: 0.9rem;
  line-height: 1.5rem;
  justify-content: center;
  color: rgba(0, 0, 0, 0.5);
  white-space: nowrap;

  @media screen and (max-width: 40rem) {
    & > span {
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
`;

const LabelColumn = styled(Column)(style);
const LabelFixedColumn = styled(FixedColumn)(style);
const LabelThinColumn = styled(ThinColumn)(style);

export default inject("t9n")(observer(Labels));
