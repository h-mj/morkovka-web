import React from "react";
import styled, { css } from "styled-components";
import { observer } from "mobx-react";

import Flex from "../../Flex";

import Column from "./Column";
import FixedColumn from "./FixedColumn";
import ThinColumn from "./ThinColumn";

function Header({ meal, deletable }) {
  if (meal.empty) {
    return <HeaderColumn>{meal.name}</HeaderColumn>;
  }

  return (
    <Flex>
      <HeaderColumn>{meal.name}</HeaderColumn>
      <HeaderFixedColumn>{meal.caloriesWithUnit}</HeaderFixedColumn>
      <HeaderFixedColumn>{meal.carbsWithUnit}</HeaderFixedColumn>
      <HeaderFixedColumn>{meal.proteinsWithUnit}</HeaderFixedColumn>
      <HeaderFixedColumn>{meal.fatsWithUnit}</HeaderFixedColumn>
      {deletable && <ThinColumn />}
    </Flex>
  );
}

const style = css`
  font-size: 1.15rem;
  font-weight: bold;
`;

const HeaderColumn = styled(Column)(style);
const HeaderFixedColumn = styled(FixedColumn)(style);

export default observer(Header);
