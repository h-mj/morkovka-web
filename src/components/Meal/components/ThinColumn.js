import styled from "styled-components";

import FixedColumn from "./FixedColumn";

export default styled(FixedColumn)`
  width: 2.5rem;
  padding: 0;
  padding-right: 1rem;
`;
