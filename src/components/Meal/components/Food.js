import React from "react";
import styled from "styled-components";
import styles from "../../../styles/constants";
import { inject, observer } from "mobx-react";

import Flex from "../../Flex";
import Remove from "../../Remove";

import Column from "./Column";
import FixedColumn from "./FixedColumn";
import ThinColumn from "./ThinColumn";

function Food({ t9n, food, deletable }) {
  return (
    <FoodRow>
      <FixedColumn>{food.quantityWithUnit}</FixedColumn>
      <Column>{food.name}</Column>
      <FixedColumn>{food.caloriesWithUnit}</FixedColumn>
      <FixedColumn>{food.carbsWithUnit}</FixedColumn>
      <FixedColumn>{food.proteinsWithUnit}</FixedColumn>
      <FixedColumn>{food.fatsWithUnit}</FixedColumn>

      {deletable && (
        <ThinColumn>
          <Remove message={t9n.t.confirm.food} remove={() => food.delete()} />
        </ThinColumn>
      )}
    </FoodRow>
  );
}

const FoodRow = styled(Flex)`
  width: 100%;
  border-bottom: solid 1px ${styles.borderColor.gray};

  &:last-child {
    border-bottom: 0;
  }
`;

export default inject("t9n")(observer(Food));
