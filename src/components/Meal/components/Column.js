import styled from "styled-components";

import styles from "../../../styles/constants";

import Flex from "../../Flex";

export default styled(Flex)`
  padding: 0 1rem;
  align-items: center;
  min-height: 3.5rem;
  color: ${styles.color.text};
  white-space: pre-wrap;
`;
