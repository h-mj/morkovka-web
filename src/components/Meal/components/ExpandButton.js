import React from "react";
import styled from "styled-components";
import styles from "../../../styles/constants";

import Flex from "../../Flex";

export default function Add({ onClick }) {
  return (
    <Container>
      <Button onClick={onClick}>+</Button>
    </Container>
  );
}

const Container = styled(Flex)`
  height: 0;
  align-items: center;
  justify-content: center;
`;

const Button = styled.div`
  width: 3.5rem;
  height: 3.5rem;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${styles.backgroundColor.primary};
  box-shadow: ${styles.boxShadow._1};

  font-size: 1.5rem;
  color: white;

  cursor: pointer;
  user-select: none;
  transition: 0.2s;

  :hover {
    background-color: ${styles.backgroundColor.primaryAction};
  }
`;
