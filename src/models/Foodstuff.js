import { extendObservable } from "mobx";
import { client } from "../utils/ApiClient";
import t9n from "./Translator";

export default class Foodstuff {
  constructor(data) {
    extendObservable(this, data);
  }

  get quantity() {
    if (this.unit === "pc") {
      return 1;
    }

    return 100;
  }

  get quantityWithUnit() {
    return this.quantity + t9n.t[this.unit.trim()];
  }

  get caloriesWithUnit() {
    return this.getFieldWithUnit("calories", false);
  }

  get carbsWithUnit() {
    return this.getFieldWithUnit("carbs");
  }

  get proteinsWithUnit() {
    return this.getFieldWithUnit("proteins");
  }

  get fatsWithUnit() {
    return this.getFieldWithUnit("fats");
  }

  getFieldWithUnit(field, grams = true) {
    return Math.round(this.quantity * this[field]) + (grams ? t9n.t.g : "");
  }

  delete = () => {
    console.log("Hello");

    return client
      .delete("/foodstuff", {
        foodstuff_id: this.id
      });
  }
}
