import { extendObservable } from "mobx";
import { client } from "../utils/ApiClient";

export default class Measurement {
  quantity = null;

  constructor(data, quantity) {
    extendObservable(this, data);

    this.quantity = quantity;
  }

  delete = () => {
    return client
      .delete("/user/quantity/measurement", {
        user_id: this.quantity.quantities.user.id,
        measurement_id: this.id
      })
      .then(data => {
        this.quantity.remove(this);
      });
  };
}
