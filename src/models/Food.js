import { computed, decorate, extendObservable, observable } from "mobx";
import { client } from "../utils/ApiClient";
import t9n from "./Translator";

export default class Food {
  meal = null;

  constructor(data, meal) {
    extendObservable(this, data);

    this.meal = meal;
  }

  get quantityWithUnit() {
    if (this.unit === "pc") {
      return parseFloat(this.quantity.toFixed(2)) + t9n.t[this.unit.trim()];
    }

    return Math.round(this.quantity) + t9n.t[this.unit.trim()];
  }

  get caloriesWithUnit() {
    return Math.round(this.calories);
  }

  get carbsWithUnit() {
    return Math.round(this.carbs) + t9n.t.g;
  }

  get proteinsWithUnit() {
    return Math.round(this.proteins) + t9n.t.g;
  }

  get fatsWithUnit() {
    return Math.round(this.fats) + t9n.t.g;
  }

  delete() {
    return client
      .delete("/user/meal/food", {
        user_id: this.meal.meals.day.user.id,
        food_id: this.id
      })
      .then(data => {
        this.meal.remove(this);
      });
  }
}

decorate(Food, {
  _meal: observable,
  quantityWithUnit: computed,
  caloriesWithUnit: computed,
  carbsWithUnit: computed,
  proteinsWithUnit: computed,
  fatsWithUnit: computed
});
