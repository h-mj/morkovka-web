import { decorate, observable } from "mobx";

export default class Stats {
  consumption = false;

  constructor(consumption, user) {
    this.consumption = consumption;
    this.user = user;
  }
}

decorate(Stats, {
  consumption: observable
});
