import { action, computed, decorate, extendObservable, observable } from "mobx";
import { client } from "../utils/ApiClient";
import t9n from "../models/Translator";

import Measurement from "./Measurement";

export default class Quantity {
  quantities = null;
  deleted = false;

  constructor(data, quantities) {
    data.measurements = data.measurements.map(
      data => new Measurement(data, this)
    );

    extendObservable(this, data);

    this.quantities = quantities;
  }

  get value() {
    if (this.measurements.length > 0) {
      return this.measurements[this.measurements.length - 1].value;
    }

    return null;
  }

  get date() {
    if (this.measurements.length > 0) {
      return this.measurements[this.measurements.length - 1].date;
    }

    return null;
  }

  get prettyName() {
    if (this.type === Quantity.CREATED) {
      return this.name;
    }

    return t9n.t.quantityNames[this.type];
  }

  get prettyValue() {
    if (this.type === Quantity.ACTIVENESS) {
      return t9n.t.activeness[Quantity.ACTIVENESS_NAMES[this.value]];
    }

    return this.value + this.prettyUnit;
  }

  get prettyUnit() {
    if (this.type === Quantity.CREATED) {
      return this.unit.trim();
    }

    return t9n.t[this.unit.trim()];
  }

  get deletable() {
    return this.type === Quantity.CREATED;
  }

  add(value) {
    return client
      .post("/user/quantity/measurement", {
        user_id: this.quantities.user.id,
        quantity_id: this.id,
        value
      })
      .then(data => {
        const index = this.findMeasurementIndexByDate(data.date);
        const measurement = new Measurement(data, this);

        if (index >= 0) {
          this.measurements[index] = measurement;
        } else {
          this.measurements.push(measurement);
        }

        this.quantities.updateMeasurements();
      });
  }

  remove(measurement) {
    const index = this.measurements.indexOf(measurement);

    if (index >= 0) {
      this.measurements.splice(index, 1);
    }

    this.quantities.updateMeasurements();
  }

  delete() {
    return client
      .delete("/user/quantity", {
        user_id: this.quantities.user.id,
        quantity_id: this.id
      })
      .then(data => {
        this.deleted = true;
        this.quantities.remove(this);
      });
  }

  findMeasurementIndexByDate(date) {
    for (let i = 0; i < this.measurements.length; ++i) {
      if (this.measurements[i].date === date) {
        return i;
      }
    }

    return -1;
  }
}

Quantity.CREATED = 0;
Quantity.MASS = 1;
Quantity.HEIGHT = 2;
Quantity.ACTIVENESS = 3;
Quantity.ACTIVENESS_NAMES = {
  0: "minimal",
  1: "low",
  2: "medium",
  3: "high",
  4: "veryHigh"
};

decorate(Quantity, {
  deleted: observable,
  value: computed,
  date: computed,
  prettyName: computed,
  prettyValue: computed,
  prettyUnit: computed,
  deletable: computed,
  add: action,
  remove: action,
  delete: action
});
