import { action, computed, extendObservable, decorate } from "mobx";
import { client } from "../utils/ApiClient";
import t9n from "./Translator";

import Food from "./Food";

export default class Meal {
  meals = null;

  constructor(data, meals) {
    this.meals = meals;

    data.foods = data.foods.map(food => new Food(food, this));
    extendObservable(this, data);
  }

  get name() {
    return t9n.t.mealNames[this.type];
  }

  get calories() {
    return this.sumValues("calories");
  }

  get carbs() {
    return this.sumValues("carbs");
  }

  get proteins() {
    return this.sumValues("proteins");
  }

  get fats() {
    return this.sumValues("fats");
  }

  get caloriesWithUnit() {
    return Math.round(this.calories);
  }

  get carbsWithUnit() {
    return Math.round(this.carbs) + t9n.t.g;
  }

  get proteinsWithUnit() {
    return Math.round(this.proteins) + t9n.t.g;
  }

  get fatsWithUnit() {
    return Math.round(this.fats) + t9n.t.g;
  }

  get empty() {
    return this.foods.length === 0;
  }

  sumValues(key) {
    let sum = 0;

    for (let i = 0; i < this.foods.length; ++i) {
      sum += this.foods[i][key];
    }

    return sum;
  }

  findFoodIndexByFoodstuffId(foodstuff_id) {
    for (let i = 0; i < this.foods.length; ++i) {
      if (this.foods[i].foodstuff_id === foodstuff_id) {
        return i;
      }
    }

    return -1;
  }

  findFoodIndexById(id) {
    for (let i = 0; i < this.foods.length; ++i) {
      if (this.foods[i].id === id) {
        return i;
      }
    }

    return -1;
  }

  add(foodstuff_id, quantity) {
    quantity = parseFloat(quantity.replace(",", "."));
    const index = this.findFoodIndexByFoodstuffId(foodstuff_id);

    if (index !== -1) {
      quantity += this.foods[index].quantity;
    }

    return client
      .post("/user/meal/food", {
        user_id: this.meals.day.user.id,
        meal_id: this.id,
        foodstuff_id: foodstuff_id,
        quantity: quantity
      })
      .then(data => {
        const food = new Food(data, this);

        if (index === -1) {
          this.foods.push(food);
        } else {
          this.foods[index] = food;
        }
      });
  }

  remove(food) {
    const index = this.findFoodIndexById(food.id);

    if (index >= 0) {
      this.foods.splice(index, 1);
    }
  }
}

decorate(Meal, {
  name: computed,
  calories: computed,
  carbs: computed,
  proteins: computed,
  fats: computed,
  empty: computed,
  add: action,
  remove: action
});
