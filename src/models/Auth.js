import { action, computed, decorate, observable } from "mobx";
import { client } from "../utils/ApiClient";

import t9n from "./Translator";
import User from "./User";
import Foodstuffs from "./Foodstuffs";

const TOKEN = "TOKEN";

export class Auth {
  _token = null;
  _user = null;
  _foodstuffs = null;

  constructor() {
    this._token = localStorage.getItem(TOKEN);
  }

  get token() {
    return this._token;
  }

  get user() {
    if (!this._user) {
      this.load();
    }

    return this._user;
  }

  get foodstuffs() {
    return this._foodstuffs;
  }

  get authenticated() {
    return this._token !== null;
  }

  login(body) {
    return client.post("/user/token", body).then(this.assign);
  }

  register(body) {
    return client.post("/user", body).then(this.assign);
  }

  load() {
    return client.get("/user/me").then(this.assign);
  }

  logout() {
    this._user = null;
    this._token = null;

    localStorage.clear();
  }

  assign = data => {
    const { token, ...user } = data;

    localStorage.setItem(TOKEN, token);

    this._token = token;
    this._user = new User(user, this);
    this._foodstuffs = new Foodstuffs();

    t9n.setLanguage(this._user.language);
  };
}

decorate(Auth, {
  _token: observable,
  _user: observable,
  _foodstuffs: observable,
  token: computed,
  user: computed,
  foodstuffs: computed,
  authenticated: computed,
  login: action,
  register: action,
  load: action,
  logout: action,
  assign: action
});

export default new Auth();
