import { action, decorate, observable } from "mobx";
import { client } from "../utils/ApiClient";

import Quantity from "../models/Quantity";

export default class Quantities {
  quantities = null;
  user = null;

  constructor(data, user) {
    this.quantities = data.map(quantity => new Quantity(quantity, this));
    this.user = user;
  }

  add(name, unit) {
    return client
      .post("/user/quantity", { user_id: this.user.id, name, unit })
      .then(data => {
        const quantity = new Quantity(data, this);
        this.quantities.push(quantity);

        return quantity;
      });
  }

  remove(quantity) {
    const index = this.quantities.indexOf(quantity);

    if (index >= 0) {
      this.quantities.splice(index, 1);
    }
  }

  updateMeasurements() {
    const mass = this.getQuantityByType(1).value;
    const height = this.getQuantityByType(2).value;
    const activeness = this.getQuantityByType(3).value;

    if (mass !== null && height !== null && activeness !== null) {
      this.user.measurements = {
        mass,
        height,
        activeness
      };
    } else {
      this.user.measurements = null;
    }
  }

  getQuantityByType(type) {
    for (let i = 0; i < this.quantities.length; ++i) {
      if (this.quantities[i].type === type) {
        return this.quantities[i];
      }
    }

    return null;
  }
}

decorate(Quantities, {
  quantities: observable,
  add: action,
  remove: action
});
