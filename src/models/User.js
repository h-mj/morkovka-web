import {
  action,
  computed,
  decorate,
  extendObservable,
  observable,
  toJS
} from "mobx";

import { client } from "../utils/ApiClient";
import moment from "moment";
import totals from "../utils/totals";

import Day from "./Day";
import Quantities from "./Quantities";
import Stats from "./Stats";
import Clients from "./Clients";

export default class User {
  _parent = null;
  _day = null;
  _quantities = null;
  _stats = null;
  _clients = null;

  _loading_day = false;
  _loading_quantities = false;
  _loading_stats = false;
  _loading_clients = false;

  constructor(data, parent) {
    this._parent = parent;
    extendObservable(this, data);
  }

  get totals() {
    return totals(this, this);
  }

  get day() {
    return this._day;
  }

  get quantities() {
    if (this._quantities === null && !this._loading_quantities) {
      this.loadQuantities();
    }

    return this._quantities;
  }

  get stats() {
    if (this._stats === null && !this._loading_stats) {
      this.loadStats();
    }

    return this._stats;
  }

  get clients() {
    if (this._clients === null && !this._loading_clients) {
      this.loadClients();
    }

    return this._clients;
  }

  loadDay(date) {
    this._day = null;
    this._loading_day = true;

    client
      .get("/user/day", {
        user_id: this.id,
        date: moment(date).format(moment.HTML5_FMT.DATE)
      })
      .then(data => {
        this._day = new Day(data, this);
        this._loading_day = false;
      });
  }

  loadQuantities() {
    this._quantities = null;
    this._loading_quantities = true;

    client.get("/user/quantities", { user_id: this.id }).then(data => {
      this._quantities = new Quantities(data, this);
      this._loading_quantities = false;
    });
  }

  loadStats() {
    this._stats = null;
    this._loading_stats = true;

    client.get("/user/stats/consumption", { user_id: this.id }).then(data => {
      this._stats = new Stats(data, this);
      this._loading_stats = false;
    });
  }

  loadClients() {
    this._clients = null;
    this._loading_stats = true;

    client.get("/user/clients", { user_id: this.id }).then(data => {
      this._clients = new Clients(data, this);
      this._loading_stats = false;
    });
  }

  update(values) {
    const map = toJS(values);
    delete map.repeated_password;

    return client.patch("/user", { user_id: this.id, ...map }).then(data => {
      this.language = values.language;
      this.email = values.email;
    });
  }

  addRatios(ratios) {
    return client.post("/user/ratios", { user_id: this.id, ...ratios });
  }

  remove(password) {
    return client.delete("/user", { user_id: this.id, password }).then(data => {
      if (this._parent instanceof Clients) {
        this._parent.remove(this);
      } else {
        this._parent.logout();
      }
    });
  }
}

decorate(User, {
  _day: observable,
  _quantities: observable,
  _stats: observable,
  _clients: observable,
  totals: computed,
  day: computed,
  quantities: computed,
  stats: computed,
  clients: computed,
  loadDay: action,
  loadQuantities: action,
  loadStats: action,
  loadClients: action,
  update: action,
  remove: action
});
