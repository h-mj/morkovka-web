import { client } from "../utils/ApiClient";

import Foodstuff from "./Foodstuff";

export default class Foodstuffs {
  find(query) {
    return client
      .get("/foodstuffs", { query })
      .then(data => data.map(foodstuff => new Foodstuff(foodstuff)));
  }

  add(foodstuff) {
    return client.post("/foodstuffs", foodstuff);
  }
}
