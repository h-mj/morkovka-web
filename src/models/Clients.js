import { decorate, observable } from "mobx";

import User from "./User";
import { client } from "../utils/ApiClient";

export default class Clients {
  clients = null;
  user = null;

  constructor(data, user) {
    this.clients = data.map(user => new User(user, this));
    this.user = user;
  }

  generateLink() {
    return client.post("/user/client/code", { user_id: this.user.id });
  }

  remove(user) {
    this.clients.remove(user);
  }
}

decorate(Clients, {
  clients: observable
});
