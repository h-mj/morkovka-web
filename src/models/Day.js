import { decorate, observable } from "mobx";

import Meals from "./Meals";

export default class Day {
  date = null;
  user = null;
  meals = null;
  ratios = null;
  measurements = null;

  constructor({ date, meals, measurements, ratios }, user) {
    this.date = date;
    this.user = user;
    this.meals = new Meals(meals, this);
    this.ratios = ratios;
    this.measurements = measurements;
  }
}

decorate(Day, {
  date: observable,
  user: observable,
  meals: observable,
  ratios: observable,
  measurements: observable
});
