import { action, decorate, observable } from "mobx";

import ee from "../translations/ee";
import ru from "../translations/ru";

const languages = {
  ee,
  ru
};

export class Translator {
  t = languages.ru;

  setLanguage(language) {
    this.t = languages[language];
  }
}

decorate(Translator, {
  t: observable,
  setLanguage: action
});

export default new Translator();
