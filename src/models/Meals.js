import { action, computed, decorate, observable } from "mobx";
import { client } from "../utils/ApiClient";

import Meal from "../models/Meal";

const typeToName = {
  breakfast: 0,
  lunch: 1,
  dinner: 2,
  snack: 3
};

export default class Meals {
  meals = null;
  day = null;

  constructor(meals, day) {
    this.meals = meals.map(meal => new Meal(meal, this));
    this.day = day;
  }

  get calories() {
    return this.valueSum("calories");
  }

  get carbs() {
    return this.valueSum("carbs");
  }

  get proteins() {
    return this.valueSum("proteins");
  }

  get fats() {
    return this.valueSum("fats");
  }

  valueSum(value) {
    let sum = 0;

    for (let i = 0; i < this.meals.length; ++i) {
      sum += this.meals[i][value];
    }

    return sum;
  }

  existsByName(name) {
    const type = typeToName[name];

    for (let i = 0; i < this.meals.length; ++i) {
      if (this.meals[i].type === type) {
        return true;
      }
    }

    return false;
  }

  add(name) {
    const type = typeToName[name];

    client
      .post("/user/meal", {
        user_id: this.day.user.id,
        type,
        date: this.day.date
      })
      .then(meal => {
        this.meals.push(new Meal(meal, this));
        this.meals = this.meals.sort((it, that) => it.type - that.type);
      })
      .catch(error => {
        console.error(error);
      });
  }
}

decorate(Meals, {
  meals: observable,
  day: observable,
  calories: computed,
  carbs: computed,
  proteins: computed,
  fats: computed,
  add: action
});
