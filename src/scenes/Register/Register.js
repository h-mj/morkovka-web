import React, { Component } from "react";
import styled from "styled-components";
import { Redirect } from "react-router-dom";
import { inject, observer } from "mobx-react";
import styles from "../../styles/constants";

import Error from "../../components/Error";
import Input from "../../components/Input";
import Select from "../../components/Select";
import Button from "../../components/Button";

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      values: {
        language: "",
        name: "",
        sex: "",
        date: "",
        month: "",
        year: "",
        email: "",
        password: "",
        code: ""
      },
      invalid: {}
    };

    document.title = "Registreeru";
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }

    const { t9n } = this.props;

    return (
      <Form onSubmit={this.register} noValidate>
        {this.state.invalid.global && (
          <Error>{t9n.t.registration.emailUsed}</Error>
        )}

        <Container>
          <Row>
            <Select
              onChange={this.change}
              value={this.state.values.language}
              invalid={this.state.invalid.language}
              name="language"
              placeholder={t9n.t.registration.language}
              options={[
                { value: "ru", name: "Русский" },
                { value: "ee", name: "eesti" }
              ]}
            />
          </Row>

          <Row>
            <Input
              onChange={this.change}
              value={this.state.values.name}
              invalid={this.state.invalid.name}
              name="name"
              type="text"
              placeholder={t9n.t.registration.name}
            />
          </Row>

          <Row>
            <Select
              onChange={this.change}
              value={this.state.values.sex}
              invalid={this.state.invalid.sex}
              name="sex"
              placeholder={t9n.t.registration.sex}
              options={[
                { value: "f", name: t9n.t.user.female },
                { value: "m", name: t9n.t.user.male }
              ]}
            />
          </Row>

          <Row>
            {["date", "month", "year"].map(value => (
              <Column key={value}>
                <Input
                  onChange={this.change}
                  value={this.state.values[value]}
                  invalid={this.state.invalid[value]}
                  name={value}
                  type="text"
                  placeholder={t9n.t.registration[value]}
                />
              </Column>
            ))}
          </Row>

          <Row>
            <Input
              onChange={this.change}
              value={this.state.values.email}
              invalid={this.state.invalid.email}
              name="email"
              type="text"
              placeholder={t9n.t.registration.email}
            />
          </Row>

          <Row>
            <Input
              onChange={this.change}
              value={this.state.values.password}
              invalid={this.state.invalid.password}
              name="password"
              type="password"
              placeholder={t9n.t.registration.password}
            />
          </Row>

          <Row>
            <Input
              onChange={this.change}
              value={this.state.values.code}
              invalid={this.state.invalid.code}
              name="code"
              type="text"
              placeholder={t9n.t.registration.code}
            />
          </Row>

          <Row>
            <Button type="submit">{t9n.t.registration.register}</Button>
          </Row>
        </Container>
      </Form>
    );
  }

  change = event => {
    const { name, value } = event.target;
    const values = this.state.values;

    values[name] = value;

    if (name === "date" || name === "month" || name === "year") {
      values.date_of_birth = `${values.year}-${values.month}-${values.date}`;
      this.setValid("date_of_birth", true, false);
    } else {
      this.setValid(name, true, false);
    }

    if (name === "language") {
      this.props.t9n.setLanguage(value);
    }

    this.setState({ values });
  };

  setValid = (names, valid, reset = true) => {
    if (!(names instanceof Array)) {
      names = [names];
    }

    const invalid = this.state.invalid;

    if (reset) {
      for (const key in invalid) {
        invalid[key] = false;
      }
    }

    for (let i = 0; i < names.length; ++i) {
      invalid[names[i]] = !valid;
    }

    invalid.date = invalid.date_of_birth;
    invalid.month = invalid.date_of_birth;
    invalid.year = invalid.date_of_birth;

    this.setState({ invalid });
  };

  register = event => {
    event.preventDefault();

    this.props.auth
      .register(this.state.values)
      .then(() => {
        this.setState({ redirect: true });
      })
      .catch(error => {
        if (error.code === 409) {
          return this.setValid("global", false);
        }

        if (error.code === 422) {
          return this.setValid(error.details.invalid, false);
        }

        console.log(error);
      });
  };
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: ${styles.boxShadow._1};
`;

const Form = styled.form`
  width: 30rem;
  margin: 4rem auto 0 auto;
`;

const Row = styled.div`
  display: flex;
  border-bottom: solid 1px ${styles.borderColor.gray};

  &:last-child {
    border-bottom: 0;
  }
`;

const Column = styled.div`
  border-right: solid 1px ${styles.borderColor.gray};

  &:last-child {
    border-right: 0;
  }
`;

export default inject("auth", "t9n")(observer(Register));
