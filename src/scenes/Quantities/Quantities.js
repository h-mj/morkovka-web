import React, { Component } from "react";
import styled from "styled-components";
import { action, decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";

import Body from "../../components/Body";
import QuantityList from "../../components/QuantityList";
import QuantityDetails from "../../components/QuantityDetails";
import QuantityAdd from "../../components/QuantityAdd";

class Quantities extends Component {
  constructor(props) {
    super(props);

    this.quantity = null;
    this.adding = false;

    document.title = props.t9n.t.navigation.quantities;
  }

  render() {
    const { quantities } = this.props.auth.user;

    if (quantities === null) {
      return null;
    }

    if (this.quantity === null) {
      this.quantity = quantities.quantities[0];
    }

    return (
      <Content>
        <QuantityList
          quantities={quantities}
          showDetails={this.showDetails}
          showAdder={this.showAdder}
        />

        <Main>
          <Body>
            {this.quantity && !this.quantity.deleted && (
              <QuantityDetails
                quantity={this.quantity}
                close={this.hideDetails}
              />
            )}

            {this.adding && (
              <QuantityAdd
                close={this.hideAdder}
                showDetails={this.showDetails}
              />
            )}
          </Body>
        </Main>
      </Content>
    );
  }

  showAdder = () => {
    this.adding = true;
  };

  hideAdder = () => {
    this.adding = false;
  };

  showDetails = quantity => {
    this.quantity = quantity;
  };

  hideDetails = () => {
    this.quantity = null;
  };
}

decorate(Quantities, {
  quantity: observable,
  adding: observable,
  showAdder: action,
  hideAdder: action,
  showDetails: action,
  hideDetails: action
});

const Content = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1 1 100%;
  min-height: 0;

  @media screen and (max-width: 40rem) {
    overflow-y: auto;
  }
`;

const Main = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;

  @media screen and (max-width: 40rem) {
    min-width: 100vw;
  }
`;

export default inject("auth", "t9n")(observer(Quantities));
