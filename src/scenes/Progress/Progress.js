import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import Body from "../../components/Body";
import RatioChanger from "../../components/RatioChanger";
import BodyMassIndexScale from "../../components/BodyMassIndexScale";
import MeasurementHistory from "../../components/MeasurementHistory";

class Progress extends Component {
  constructor(props) {
    super(props);

    document.title = props.t9n.t.navigation.progress;
  }

  render() {
    const user = this.props.auth.user;
    const quantities = user.quantities;

    if (!quantities) {
      return null;
    }

    return (
      <Body>
        <RatioChanger user={this.props.auth.user} />
        <BodyMassIndexScale user={this.props.auth.user} />
        <MeasurementHistory quantities={quantities} />
      </Body>
    );
  }
}

export default inject("auth", "t9n")(observer(Progress));
