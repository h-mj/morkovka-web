import React, { Component } from "react";
import styled from "styled-components";
import styles from "../../styles/constants";
import fadeIn from "../../styles/fadeIn";
import { action, decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";

import Body from "../../components/Body";
import Error from "../../components/Error";
import Container from "../../components/Container";
import ActionButton from "../../components/ActionButton";
import Input from "../../components/Input";
import Select from "../../components/Select";
import SectionTitle from "../../components/SectionTitle";
import Section from "../../components/Section";
import Subsection from "../../components/Subsection";
import Separator from "../../components/Separator";
import Remove from "../../components/Remove";

class Settings extends Component {
  constructor(props) {
    super(props);

    this.values = {
      language: props.auth.user.language,
      email: props.auth.user.email,
      new_password: "",
      repeated_password: "",
      password: ""
    };

    this.invalid = {
      language: false,
      email: false,
      new_password: false,
      repeated_password: false,
      password: false
    };

    this.saved = false;
    this.emailTaken = false;

    document.title = props.t9n.t.navigation.settings;
  }

  render() {
    const { t9n } = this.props;

    return (
      <Body>
        <Subsection>
          <Placeholder>
            {this.saved && <Saved>{t9n.t.saved}</Saved>}
            {this.emailTaken && <Error>{t9n.t.registration.emailUsed}</Error>}
          </Placeholder>
        </Subsection>

        <Subsection>
          <Section>
            <SectionTitle>{t9n.t.settings.language}</SectionTitle>
            <Container>
              <Select
                value={this.values.language}
                onChange={this.change}
                name="language"
                placeholder={t9n.t.settings.language}
                options={[
                  { value: "ru", name: "Русский" },
                  { value: "ee", name: "eesti" }
                ]}
              />
            </Container>
          </Section>

          <Section>
            <SectionTitle>{t9n.t.settings.email}</SectionTitle>
            <Container>
              <Input
                value={this.values.email}
                invalid={this.invalid.email}
                onChange={this.change}
                name="email"
                placeholder={t9n.t.settings.email}
              />
            </Container>
          </Section>

          <Section>
            <SectionTitle>{t9n.t.settings.newPassword}</SectionTitle>
            <Container>
              <Input
                value={this.values.new_password}
                invalid={this.invalid.new_password}
                onChange={this.change}
                name="new_password"
                placeholder={t9n.t.settings.newPassword}
                type="password"
              />

              <Separator />

              <Input
                value={this.values.repeated_password}
                invalid={this.invalid.repeated_password}
                onChange={this.change}
                name="repeated_password"
                placeholder={t9n.t.settings.repeatPassword}
                type="password"
              />
            </Container>
          </Section>

          <Section>
            <SectionTitle>{t9n.t.settings.currentPassword}</SectionTitle>
            <Container>
              <Input
                value={this.values.password}
                invalid={this.invalid.password}
                onChange={this.change}
                name="password"
                placeholder={t9n.t.settings.currentPassword}
                type="password"
              />
            </Container>
          </Section>
        </Subsection>

        <Section>
          <Container>
            <ActionButton onClick={this.submit}>
              {t9n.t.settings.save}
            </ActionButton>
          </Container>
        </Section>

        <Section>
          <Remove
            message={t9n.t.confirm.user}
            remove={password => this.props.auth.user.remove(password)}
            withPassword
          >
            {t9n.t.deleteAccount}
          </Remove>
        </Section>
      </Body>
    );
  }

  componentWillUnmount() {
    const { user } = this.props.auth;

    if (user) {
      this.props.t9n.setLanguage(user.language);
    }
  }

  change = event => {
    const { name, value } = event.target;

    this.values[name] = value;
    this.setValid(name, true, false);
    this.saved = false;

    if (name === "language") {
      this.props.t9n.setLanguage(value);
      document.title = this.props.t9n.t.navigation.settings;
    }

    if (name === "email") {
      this.emailTaken = false;
    }
  };

  setValid = (names, valid, reset = true) => {
    if (!(names instanceof Array)) {
      names = [names];
    }

    if (reset) {
      for (const key in this.invalid) {
        this.invalid[key] = false;
      }
    }

    for (let i = 0; i < names.length; ++i) {
      this.invalid[names[i]] = !valid;
    }
  };

  submit = event => {
    const { user } = this.props.auth;

    if (
      this.values.language === user.language &&
      this.values.email === user.email &&
      this.values.new_password === "" &&
      this.values.new_password === this.values.repeated_password
    ) {
      return this.clear();
    }

    if (!this.values.password) {
      return this.setValid("password", false);
    }

    if (
      this.values.new_password &&
      this.values.new_password !== this.values.repeated_password
    ) {
      return this.setValid(["new_password", "repeated_password"], false);
    }

    user
      .update(this.values)
      .then(() => {
        this.clear();
      })
      .catch(error => {
        if (error.code === 400) {
          return this.setValid("password", false);
        }

        if (error.code === 409) {
          this.emailTaken = true;
          return this.setValid("email", false);
        }

        if (error.code === 422) {
          return this.setValid(error.details.invalid, false);
        }

        console.log(error);
      });
  };

  clear = () => {
    const { user } = this.props.auth;

    this.values = {
      language: user.language,
      email: user.email,
      new_password: "",
      repeated_password: "",
      password: ""
    };

    this.setValid([], true);
    this.saved = true;
    this.emailTaken = false;

    setTimeout(() => {
      this.saved = false;
    }, 3000);
  };
}

decorate(Settings, {
  values: observable,
  invalid: observable,
  saved: observable,
  emailTaken: observable,
  change: action,
  setValid: action,
  submit: action,
  clear: action
});

const Placeholder = styled.div`
  width: 100%;
  height: 3.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Saved = styled.div`
  color: ${styles.color.textAction};
  text-align: center;
  font-size: 1.2rem;
  animation: ${fadeIn} 0.2s;
`;

export default inject("t9n", "auth")(observer(Settings));
