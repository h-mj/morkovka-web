import React, { Component } from "react";
import moment from "moment";
import { action, decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";

import Body from "../../components/Body";
import Tabs from "../../components/Tabs";
import Section from "../../components/Section";
import Subsection from "../../components/Subsection";
import Progresses from "../../components/Progresses";
import MealAdd from "../../components/MealAdd";
import Meal from "../../components/Meal";
import FoodAdd from "../../components/FoodAdd";

class Counter extends Component {
  constructor(props) {
    super(props);

    let day = new Date();

    this.days = [];

    for (let i = 0; i < 7; ++i) {
      this.days.unshift(new Date(day));
      day.setDate(day.getDate() - 1);
    }

    this.tabs = this.days.map(day => {
      return `${props.t9n.t.days[day.getDay()]} (${moment(day).format(
        "DD.MM"
      )})`;
    });

    this.meal = null;
    this.select(this.tabs.length - 1);

    document.title = props.t9n.t.navigation.counter;
  }

  render() {
    const day = this.props.auth.user.day;

    return (
      <div>
        <Tabs tabs={this.tabs} selected={this.selected} select={this.select} />

        {day && (
          <Body>
            <Section>
              <Subsection>
                <Progresses day={day} />
              </Subsection>

              <Subsection>
                <MealAdd meals={day.meals} />
              </Subsection>
            </Section>

            {day.meals.meals.map((meal, index) => (
              <Meal
                key={meal.id.toString()}
                meal={meal}
                addFood={this.addFood}
              />
            ))}

            {this.meal && (
              <FoodAdd
                meal={this.meal}
                close={this.closeFoodAdd}
                openFoodstuff={this.openFoodstuff}
              />
            )}
          </Body>
        )}
      </div>
    );
  }

  select = index => {
    if (this.selected === index) {
      return;
    }

    this.selected = index;
    this.props.auth.user.loadDay(
      this.days[this.selected].toISOString().split("T")[0]
    );
  };

  addFood = meal => {
    this.meal = meal;
  };

  closeFoodAdd = () => {
    this.meal = null;
  };
}

decorate(Counter, {
  selected: observable,
  meal: observable,
  select: action,
  addFood: action,
  closeFoodAdd: action
});

export default inject("auth", "t9n")(observer(Counter));
