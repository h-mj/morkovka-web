import React, { Component } from "react";
import styled from "styled-components";
import { decorate, observable, action } from "mobx";
import { inject, observer } from "mobx-react";
import totals from "../../utils/totals";

import Body from "../../components/Body";
import Tabs from "../../components/Tabs";
import ClientList from "../../components/ClientList";
import RatioChanger from "../../components/RatioChanger";
import BodyMassIndexScale from "../../components/BodyMassIndexScale";
import ConsumptionHistory from "../../components/ConsumptionHistory";
import MeasurementHistory from "../../components/MeasurementHistory";

class Clients extends Component {
  constructor(props) {
    super(props);

    this.selected = 0;
    this.client = null;
    this.data = null;

    this.tabs = [
      props.t9n.t.navigation.progress,
      props.t9n.t.navigation.history
    ];

    document.title = props.t9n.t.navigation.clients;
  }

  render() {
    const user = this.props.auth.user;

    if (user.type !== 1) {
      return null;
    }

    const { clients } = user;

    if (clients === null) {
      return null;
    }

    if (this.client === null && clients.clients.length > 0) {
      this.select(clients.clients[0]);
    }

    if (!this.client || !this.client.stats || !this.client.quantities) {
      return (
        <Content>
          <ClientList clients={clients} select={this.select} />
        </Content>
      );
    }

    const data = this.client.stats.consumption;
    const quantities = this.client.quantities;

    for (let i = 0; i < data.length; ++i) {
      data[i].totals = totals(this.client, data[i]);
    }

    return (
      <Content>
        <ClientList
          clients={clients}
          select={this.select}
          client={this.client}
        />

        {this.client && (
          <Main>
            <Tabs
              tabs={this.tabs}
              selected={this.selected}
              select={this.selectTab}
            />

            {this.selected === 0 && (
              <Body>
                <RatioChanger user={this.client} onChange={this.update} />
                <BodyMassIndexScale user={this.client} />
                <MeasurementHistory quantities={quantities} />
              </Body>
            )}

            {this.selected === 1 && (
              <Body>
                <ConsumptionHistory user={this.client} data={data} />
              </Body>
            )}
          </Main>
        )}
      </Content>
    );
  }

  selectTab = index => {
    this.selected = index;
  };

  select = client => {
    this.client = client;
    this.client.loadStats();
  };

  update = ratios => {
    const data = this.client.stats.consumption;
    const last = data[data.length - 1];

    last.ratios = ratios;
    last.totals = totals(this.client, last);
  };
}

decorate(Clients, {
  selected: observable,
  client: observable,
  selectTab: action,
  select: action,
  update: action
});

const Content = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1 1 100%;
  min-height: 0;

  @media screen and (max-width: 40rem) {
    overflow-y: auto;
  }
`;

const Main = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;

  @media screen and (max-width: 40rem) {
    min-width: 100vw;
  }
`;

export default inject("auth", "t9n")(observer(Clients));
