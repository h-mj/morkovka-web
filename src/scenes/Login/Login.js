import React, { Component } from "react";
import styled from "styled-components";
import { Redirect } from "react-router-dom";
import { inject, observer } from "mobx-react";

import Error from "../../components/Error";
import { Input } from "../../next/component/Input";
import { Button } from "../../next/component/Button";
import { declarations } from "../../next/style/declarations";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      values: {
        email: "",
        password: ""
      },
      invalid: {
        global: false,
        emai: false,
        password: false
      },
      redirect: false
    };

    document.title = "Logi sisse";
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }

    return (
      <Div>
        <Form onSubmit={this.login} noValidate>
          <Title>Morkovka</Title>
          <CustomError>
            {this.state.invalid.global && "E-posti aadress ja parool ei kattu."}
          </CustomError>

          <Input
            onChange={this.change}
            value={this.state.values.email}
            error={
              this.state.invalid.email
                ? "Sisestage e-posti aadress."
                : undefined
            }
            name="email"
            type="email"
            placeholder="E-posti aadress"
          />

          <Input
            onChange={this.change}
            value={this.state.values.password}
            error={
              this.state.invalid.password ? "Sisestage parool." : undefined
            }
            name="password"
            type="password"
            placeholder="Parool"
          />

          <Button
            type="submit"
            hasError={
              this.state.invalid.emai ||
              this.state.invalid.password ||
              this.state.invalid.global
            }
          >
            Logi sisse
          </Button>
        </Form>
      </Div>
    );
  }

  change = event => {
    const { name, value } = event.target;

    const values = this.state.values;
    const invalid = this.state.invalid;

    values[name] = value;
    invalid[name] = false;
    invalid.global = false;

    this.setState({ values, invalid });
  };

  setValid = (names, valid) => {
    if (!(names instanceof Array)) {
      names = [names];
    }

    const invalid = this.state.invalid;

    for (const key in invalid) {
      invalid[key] = false;
    }

    for (let i = 0; i < names.length; ++i) {
      invalid[names[i]] = !valid;
    }

    this.setState({ invalid });
  };

  login = event => {
    event.preventDefault();

    if (!this.state.values.email && !this.state.values.password) {
      return this.setValid(["email", "password"], false);
    } else if (!this.state.values.email) {
      return this.setValid("email", false);
    } else if (!this.state.values.password) {
      return this.setValid("password", false);
    }

    this.props.auth
      .login(this.state.values)
      .then(() => {
        this.setState({ redirect: true });
      })
      .catch(error => {
        if (error.code === 400) {
          return this.setValid("global", false);
        }

        if (error.code === 422) {
          return this.setValid(error.details.invalid, false);
        }

        console.log(error);
      });
  };
}

const Div = styled.div`
  max-width: 32rem;
  height: 100%;
  overflow-y: auto;
  box-sizing: border-box;
  background-color: white;
  box-shadow: 0 0 1rem rgba(0, 0, 0, 0.16);

  @media screen and (max-width: 40rem) {
    max-width: 100%;
    box-shadow: none;
  }
`;

const CustomError = styled(Error)`
  width: 100%;
  height: 1rem;
  font-weight: 300;
  color: ${declarations.color.error};
  display: flex;
  align-items: center;
  margin-bottom: 2rem;
  justify-content: center;
`;

const Title = styled.div`
  width: 100%;
  height: 2.5rem;
  font-size: 1.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Form = styled.form`
  width: 100%;
  max-width: 30rem;
  padding: 2rem;
  box-sizing: border-box;
  margin: 4rem auto;
`;

export default inject("auth")(observer(Login));
