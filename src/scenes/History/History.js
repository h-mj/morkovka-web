import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import totals from "../../utils/totals";

import Body from "../../components/Body";
import ConsumptionHistory from "../../components/ConsumptionHistory";

class History extends Component {
  constructor(props) {
    super();

    props.auth.user.loadStats();

    document.title = props.t9n.t.navigation.history;
  }

  render() {
    const user = this.props.auth.user;
    const stats = user.stats;

    if (stats === null) {
      return null;
    }

    const data = stats.consumption;

    for (let i = 0; i < data.length; ++i) {
      const set = data[i];

      if (set.ratios && set.measurements) {
        set.totals = totals(user, set);
      } else {
        set.totals = null;
      }
    }

    return (
      <Body>
        <ConsumptionHistory user={user} data={data} />
      </Body>
    );
  }
}

export default inject("auth", "t9n")(observer(History));
