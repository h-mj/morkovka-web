import React from "react";
import { inject, observer } from "mobx-react";
import { Redirect } from "react-router-dom";

const Home = ({ auth }) => {
  if (auth.authenticated) {
    return <Redirect to="/counter" />;
  }

  return <Redirect to="/login" />;
};

export default inject("auth")(observer(Home));
