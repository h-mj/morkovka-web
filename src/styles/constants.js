export default {
  color: {
    text: "rgba(0, 0, 0, 0.7)",
    textDark: "rgba(0, 0, 0, 9)",
    textLight: "rgba(0, 0, 0, 0.5)",
    textAction: "#119527"
  },
  backgroundColor: {
    slate: "#dddddd",
    slateAction: "#d0d0d0",
    primary: "#11aa33",
    primaryAction: "#119527",
    primaryLight: "#11aa3333"
  },
  borderColor: {
    gray: "rgba(0, 0, 0, 0.2)"
  },
  boxShadow: {
    _1: "0 2px 3px rgba(0, 0, 0, 0.2)"
  }
};
