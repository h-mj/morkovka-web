export default function round(value, places = 0) {
  const exponent = Math.pow(10, places);

  return Math.round(exponent * value) / exponent;
}
