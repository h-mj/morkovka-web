import moment from "moment";

const activenessMultipliers = {
  0: 1.2,
  1: 1.375,
  2: 1.55,
  3: 1.725,
  4: 1.9
};

export default function totals(user, day) {
  const { date, ratios, measurements } = day;

  if (!ratios || !measurements) {
    return null;
  }

  const { sex, date_of_birth } = user;
  const { mass, height, activeness } = measurements;
  const { delta, carbs, proteins, fats } = ratios;
  const age = moment(date).diff(moment(date_of_birth), "years");
  const activenessMultiplier = activenessMultipliers[activeness];

  let calories;

  if (sex === "f") {
    calories =
      (655 + 9.5 * mass + 1.9 * height - 4.7 * age) * activenessMultiplier;
  } else {
    calories =
      (66 + 13.8 * mass + 5.0 * height - 6.8 * age) * activenessMultiplier;
  }

  const totalCalories = calories * (100 + delta) / 100;
  const totalCarbs = totalCalories * carbs / 100 / 4;
  const totalProteins = totalCalories * proteins / 100 / 4;
  const totalFats = totalCalories * fats / 100 / 9;

  return {
    defaultCalories: calories,
    calories: totalCalories,
    carbs: totalCarbs,
    proteins: totalProteins,
    fats: totalFats
  };
}
