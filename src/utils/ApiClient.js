import auth from "../models/Auth";

export default class ApiClient {
  fetch(path, method, data) {
    let body;

    if (method === "GET") {
      path = data ? path + "?" + this.createQueryString(data) : path;
    } else {
      body = data ? JSON.stringify(data) : undefined;
    }

    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (auth && auth.authenticated) {
      headers["Authorization"] = "Bearer " + auth.token;
    }

    const request = fetch("/api" + path, {
      method: method,
      body,
      headers
    });

    return new Promise((resolve, reject) => {
      request
        .then(response => response.json())
        .then(
          json => {
            const { data, error } = json;

            if (error) {
              throw error;
            }

            return resolve(data);
          },
          error => {
            throw error;
          }
        )
        .catch(error => {
          if (error.code === 401) {
            auth.logout();
          }

          return reject(error);
        });
    });
  }

  get(path, data) {
    return this.fetch(path, "GET", data);
  }

  post(path, data) {
    return this.fetch(path, "POST", data);
  }

  patch(path, data) {
    return this.fetch(path, "PATCH", data);
  }

  delete(path, data) {
    return this.fetch(path, "DELETE", data);
  }

  createQueryString = data => {
    return Object.keys(data)
      .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`)
      .join("&");
  };
}

export const client = new ApiClient();
