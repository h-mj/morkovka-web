import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "mobx-react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { Chart } from "chart.js";
import * as ChartDataLabels from "chartjs-plugin-datalabels";

import Auth from "./components/Auth";
import Navigation from "./components/Navigation";
import Home from "./scenes/Home";
import Login from "./scenes/Login";
import Register from "./scenes/Register";
import Counter from "./scenes/Counter";
import Quantities from "./scenes/Quantities";
import Progress from "./scenes/Progress";
import History from "./scenes/History";
import Clients from "./scenes/Clients";
import Settings from "./scenes/Settings";

import auth from "./models/Auth";
import translator from "./models/Translator";

Chart.defaults.global.defaultFontFamily = '"Inter UI", sans-serif';
Chart.defaults.global.defaultFontSize = 14;
Chart.plugins.unregister(ChartDataLabels);

ReactDOM.render(
  <Provider auth={auth} t9n={translator}>
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />

        <Auth>
          <Navigation />

          <Switch>
            <Route exact path="/counter" component={Counter} />
            <Route exact path="/quantities" component={Quantities} />
            <Route exact path="/progress" component={Progress} />
            <Route exact path="/history" component={History} />
            <Route exact path="/clients" component={Clients} />
            <Route exact path="/settings" component={Settings} />
          </Switch>
        </Auth>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
